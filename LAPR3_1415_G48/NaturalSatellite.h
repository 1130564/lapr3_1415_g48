/*
*
* @author Vitor Moreira <1130564@isep.ipp.pt>
* @author Paulo Andrade <1130313@isep.ipp.pt>
* @author Emanuel Marques <1130553@isep.ipp.pt>
* @author Ivo Joao <1130404@isep.ipp.pt>
* @author Aria Jozi <1130777@isep.ipp.pt>
* @version 1.0
*
*/

#ifndef NATURALSATELLITE_H
#define	NATURALSATELLITE_H

#include "Body.h"


using namespace std;

class NaturalSatellite : public Body {
private:

    double period_of_revolution;
    double orbital_speed;
    double incl_of_axis_to_orbit;
    double equa_diameter;
    double density;
    double escape_velocity;
    double semimajor_axis;
    double orbit_eccentricity;
    double orbit_inclination;
    double perihelion;
    double aphelion;
    double meanAnomaly;
    double longitudeOfPerihelion;
    double longitudeOfAscendingNode;

public:

    NaturalSatellite();
    NaturalSatellite(int id, string name, double mass, vetor initialVelocity, vetor initialPosition, double period_of_revolution, double orbital_speed, double incl_of_axis_to_orbit, double equa_diameter, double density, double escape_velocity, double semimajor_axis, double orbit_eccentricity, double orbit_inclination, double perihelion, double aphelion, double meanAnomaly, double longitudeOfPerihelion, double longitudeOfAscendingNode);
    NaturalSatellite(string name, double mass, vetor initialVelocity, vetor initialPosition, double period_of_revolution, double orbital_speed, double incl_of_axis_to_orbit, double equa_diameter, double density, double escape_velocity, double semimajor_axis, double orbit_eccentricity, double orbit_inclination, double perihelion, double aphelion, double meanAnomaly, double longitudeOfPerihelion, double longitudeOfAscendingNode);
    NaturalSatellite(const NaturalSatellite& toCopy);

    virtual ~NaturalSatellite();
    virtual NaturalSatellite * clone() const = 0;

    double getPeriodOfRevolution() const;
    double getOrbitalSpeed() const;
    double getInclOfAxisToOrbit() const;
    double getEquaDiameter() const;
    double getDensity() const;
    double getEscapeVelocity() const;
    double getSemimajorAxis() const;
    double getOrbitEccentricity() const;
    double getOrbitInclination() const;
    double getPerihelion() const;
    double getAphelion() const;

    void setPeriodOfRevolution(double a);
    void setOrbitalSpeed(double a);
    void setInclOfAxisToOrbit(double a);
    void setEquaDiameter(double a);
    void setDensity(double a);
    void setEscapeVelocity(double a);
    void setSemimajorAxis(double a);
    void setOrbitEccentricity(double a);
    void setOrbitInclination(double a);
    void setPerihelion(double a);
    void setAphelion(double a);

    virtual void escreve(ostream& ostr) const;
    void setLongitudeOfAscendingNode(double longitudeOfAscendingNode);
    double getLongitudeOfAscendingNode() const;
    void setLongitudeOfPerihelion(double longitudeOfPerihelion);
    double getLongitudeOfPerihelion() const;
    void setMeanAnomaly(double meanAnomaly);
    double getMeanAnomaly() const;
};

NaturalSatellite::NaturalSatellite() {


}

NaturalSatellite::NaturalSatellite(int id1, string name, double mass, vetor initialVelocity, vetor initialPosition, double period_of_revolution, double orbital_speed, double incl_of_axis_to_orbit, double equa_diameter, double density, double escape_velocity, double semimajor_axis, double orbit_eccentricity, double orbit_inclination, double perihelion, double aphelion, double meanAnomaly, double longitudeOfPerihelion, double longitudeOfAscendingNode) : Body(id1, name, mass, initialVelocity, initialPosition) {
    this->period_of_revolution = period_of_revolution;
    this->orbital_speed = orbital_speed;
    this->incl_of_axis_to_orbit = incl_of_axis_to_orbit;
    this->equa_diameter = equa_diameter;
    this->density = density;
    this->escape_velocity = escape_velocity;
    this->semimajor_axis = semimajor_axis;
    this->orbit_eccentricity = orbit_eccentricity;
    this->orbit_inclination = orbit_inclination;
    this->perihelion = perihelion;
    this->aphelion = aphelion;
    this->meanAnomaly = meanAnomaly;
    this->longitudeOfPerihelion = longitudeOfPerihelion;
    this->longitudeOfAscendingNode = longitudeOfAscendingNode;
}

NaturalSatellite::NaturalSatellite(string name, double mass, vetor initialVelocity, vetor initialPosition, double period_of_revolution, double orbital_speed, double incl_of_axis_to_orbit, double equa_diameter, double density, double escape_velocity, double semimajor_axis, double orbit_eccentricity, double orbit_inclination, double perihelion, double aphelion, double meanAnomaly, double longitudeOfPerihelion, double longitudeOfAscendingNode) : Body(name, mass, initialVelocity, initialPosition) {
    this->period_of_revolution = period_of_revolution;
    this->orbital_speed = orbital_speed;
    this->incl_of_axis_to_orbit = incl_of_axis_to_orbit;
    this->equa_diameter = equa_diameter;
    this->density = density;
    this->escape_velocity = escape_velocity;
    this->semimajor_axis = semimajor_axis;
    this->orbit_eccentricity = orbit_eccentricity;
    this->orbit_inclination = orbit_inclination;
    this->perihelion = perihelion;
    this->aphelion = aphelion;
    this->meanAnomaly = meanAnomaly;
    this->longitudeOfPerihelion = longitudeOfPerihelion;
    this->longitudeOfAscendingNode = longitudeOfAscendingNode;
}

NaturalSatellite::NaturalSatellite(const NaturalSatellite& toCopy) : Body(toCopy) {

    this->period_of_revolution = toCopy.period_of_revolution;
    this->orbital_speed = toCopy.orbital_speed;
    this->incl_of_axis_to_orbit = toCopy.incl_of_axis_to_orbit;
    this->equa_diameter = toCopy.equa_diameter;
    this->density = toCopy.density;
    this->escape_velocity = toCopy.escape_velocity;
    this->semimajor_axis = toCopy.semimajor_axis;
    this->orbit_eccentricity = toCopy.orbit_eccentricity;
    this->orbit_inclination = toCopy.orbit_inclination;
    this->perihelion = toCopy.perihelion;
    this->aphelion = toCopy.aphelion;
    this->meanAnomaly = toCopy.meanAnomaly;
    this->longitudeOfPerihelion = toCopy.longitudeOfPerihelion;
    this->longitudeOfAscendingNode = toCopy.longitudeOfAscendingNode;
}

NaturalSatellite::~NaturalSatellite() {


}

double NaturalSatellite::getPeriodOfRevolution() const {
    return this->period_of_revolution;
}

double NaturalSatellite::getOrbitalSpeed() const {
    return this->orbital_speed;
}

double NaturalSatellite::getInclOfAxisToOrbit() const {
    return this->incl_of_axis_to_orbit;
}

double NaturalSatellite::getEquaDiameter() const {
    return this->equa_diameter;
}

double NaturalSatellite::getDensity() const {
    return this->density;
}

double NaturalSatellite::getEscapeVelocity() const {
    return this->escape_velocity;
}

double NaturalSatellite::getSemimajorAxis() const {
    return this->semimajor_axis;
}

double NaturalSatellite::getOrbitEccentricity() const {
    return this->orbit_eccentricity;
}

double NaturalSatellite::getOrbitInclination() const {
    return this->orbit_inclination;
}

double NaturalSatellite::getPerihelion() const {
    return this->perihelion;
}

double NaturalSatellite::getAphelion() const {
    return this->aphelion;
}

void NaturalSatellite::setPeriodOfRevolution(double a) {
    this->period_of_revolution = a;
}

void NaturalSatellite::setOrbitalSpeed(double a) {
    this->orbital_speed = a;
}

void NaturalSatellite::setInclOfAxisToOrbit(double a) {
    this->incl_of_axis_to_orbit = a;
}

void NaturalSatellite::setEquaDiameter(double a) {
    this->equa_diameter = a;
}

void NaturalSatellite::setDensity(double a) {
    this->density = a;
}

void NaturalSatellite::setEscapeVelocity(double a) {
    this->escape_velocity = a;
}

void NaturalSatellite::setSemimajorAxis(double a) {
    this->semimajor_axis = a;
}

void NaturalSatellite::setOrbitEccentricity(double a) {
    this->orbit_eccentricity = a;
}

void NaturalSatellite::setOrbitInclination(double a) {
    this->orbit_inclination = a;
}

void NaturalSatellite::setPerihelion(double a) {
    this->perihelion = a;
}

void NaturalSatellite::setAphelion(double a) {
    this->aphelion = a;
}

void NaturalSatellite::escreve(ostream& ostr) const {
    Body::escreve(ostr);
    ostr << "Period of Revolution: " << this->period_of_revolution << endl;
    ostr << "Orbital speed: " << this->orbital_speed << endl;
    ostr << "Inclination of axis to orbi: " << this->incl_of_axis_to_orbit << endl;
    ostr << "Equatorial diameter: " << this->equa_diameter << endl;
    ostr << "Density: " << this->density << endl;
    ostr << "Escape Velocity: " << this->escape_velocity << endl;
    ostr << "Semimajor axis: " << this->semimajor_axis << endl;
    ostr << "Orbit eccentricity: " << this->orbit_eccentricity << endl;
    ostr << "Orbit inclination: " << this->orbit_inclination << endl;
    ostr << "Perihelion: " << this->perihelion << endl;
    ostr << "Aphelion: " << this->aphelion << endl;
}

void NaturalSatellite::setLongitudeOfAscendingNode(double longitudeOfAscendingNode) {
    this->longitudeOfAscendingNode = longitudeOfAscendingNode;
}

double NaturalSatellite::getLongitudeOfAscendingNode() const {
    return longitudeOfAscendingNode;
}

void NaturalSatellite::setLongitudeOfPerihelion(double longitudeOfPerihelion) {
    this->longitudeOfPerihelion = longitudeOfPerihelion;
}

double NaturalSatellite::getLongitudeOfPerihelion() const {
    return longitudeOfPerihelion;
}

void NaturalSatellite::setMeanAnomaly(double meanAnomaly) {
    this->meanAnomaly = meanAnomaly;
}

double NaturalSatellite::getMeanAnomaly() const {
    return meanAnomaly;
}

#endif	/* NATURALSATELLITE_H */

