/*
*
* @author Vitor Moreira <1130564@isep.ipp.pt>
* @author Paulo Andrade <1130313@isep.ipp.pt>
* @author Emanuel Marques <1130553@isep.ipp.pt>
* @author Ivo Joao <1130404@isep.ipp.pt>
* @author Aria Jozi <1130777@isep.ipp.pt>
* @version 1.0
*
*/

#ifndef EDITSOLARSYSTEMUI_H
#define	EDITSOLARSYSTEMUI_H

using namespace std;


#include "EditSolarSystemController.h"
#include "Moon.h"
#include "Planet.h"
#include "OtherBody.h"
#include <vector>
#include <list>

class EditSolarSystemUI {
private:

    System * sistema;
    EditSolarSystemController controller;

public:

    EditSolarSystemUI(System * sistema);
    ~EditSolarSystemUI();

    bool addObjectsQuestion();
    void addObjects();
    bool editSolarSystemQuestion();
    void editSolarSystem();
    bool moonAroundAnotherObjectQuestion();
    bool otherBodyAroundPlanetQuestion();
    bool showSolarSystems();
    bool showBodiesAndAddMoon(Moon * m);
    bool showBodiesAndAddOtherBody(OtherBody * ob);
    void pedeInformacao();
    bool pedeConfirmacao();
    void mostraNovoSistema();
    void run();
    int menu();
};

EditSolarSystemUI::EditSolarSystemUI(System * sistema) : controller(sistema) {
    this->sistema = sistema;

}

EditSolarSystemUI::~EditSolarSystemUI() {

}

void EditSolarSystemUI::pedeInformacao() {
    string ss_name;
    cout << "Please, insert the Solar System's name: \n";
    cin >> ss_name;
    cout << endl;
}

bool EditSolarSystemUI::pedeConfirmacao() {
    string resposta;
    do {
        cout << "\nDo you confirm the creation of the solar system? y/n" << endl;
        cin >> resposta;
    } while (resposta != "y" && resposta != "n");

    if (resposta == "y") {
        return true;
    } else {
        return false;
    }

}

bool EditSolarSystemUI::moonAroundAnotherObjectQuestion() {
    string resposta;
    do {
        cout << "\nDo you want to make the moon go around another moon or planet ? y/n" << endl;
        cin >> resposta;
    } while (resposta != "y" && resposta != "n");

    if (resposta == "y") {
        return true;
    } else {
        return false;
    }

}

bool EditSolarSystemUI::otherBodyAroundPlanetQuestion() {
     string resposta;
    do {
        cout << "\nDo you want to make the body go around another planet ? y/n" << endl;
        cin >> resposta;
    } while (resposta != "y" && resposta != "n");

    if (resposta == "y") {
        return true;
    } else {
        return false;
    }

}



bool EditSolarSystemUI::addObjectsQuestion() {
    string resposta;
    do {
        cout << "\nDo you want to add objects to the Solar System? y/n" << endl;
        cin >> resposta;
    } while (resposta != "y" && resposta != "n");

    if (resposta == "y") {
        return true;
    } else {
        return false;
    }

}

int EditSolarSystemUI::menu() {
    int object;
    cout << "Select the object you want to create" << endl;
    cout << " 1- Moon\n"
            " 2- Planet\n"
            " 3- OtherBody\n"
            " 4- Exit\n" << endl;
    cin >> object;

    return object;
}



void EditSolarSystemUI::addObjects() {
	cout << "\nInsert the name of the CSV file that contains the bodies information:" << endl;
	string file;
	cin >> file;
	cout << "\nInsert the type of the CSV file" << endl;
	cout << "1 - CsvReaderType I" << endl;
	cout << "2 - CsvReaderType II" << endl;
	cout << "3 - CsvReaderType III" << endl;

	int escolha_csv;
	do {
		cin >> escolha_csv;
	} while (escolha_csv < 0 || escolha_csv > 3);

	InputInterface * ip;
	switch (escolha_csv) {
	case 1:
	{
		ip = new CsvReader(sistema, controller.getSolarSystem());
		controller.setObjects(ip->read(file));
		break;
	}
	case 2:
	{
		ip = new CsvReaderTypeII(sistema, controller.getSolarSystem());
		controller.setObjects(ip->read(file));

		break;
	}
	case 3:
	{
		ip = new CsvReaderTypeIII(sistema, controller.getSolarSystem());
		controller.setObjects(ip->read(file));
		break;
	}
	}

	if (pedeConfirmacao()) {
		cout << "\nSolar system edited with sucess!" << endl;
		cout << "Press any key to continue..." << endl;
		char s;
		cin >> s;
	}
	
}
bool EditSolarSystemUI::editSolarSystemQuestion() {
    string resposta;
    do {
        cout << "\nDo you want to edit Solar System objects? y/n" << endl;
        cin >> resposta;
    } while (resposta != "y" && resposta != "n");

    if (resposta == "y") {
        return true;
    } else {
        return false;
    }

}

void EditSolarSystemUI::editSolarSystem() {
    string starName;
    float starMass;
    string solarSystemName;
    string resposta;

    do {
        cout << "\nDo you want to change the star name ? y/n" << endl;
        cin >> resposta;
    } while (resposta != "y" && resposta != "n");

    if (resposta == "y") {
        cout << "\nInsert the star's new name\n" << endl;
        cin >> starName;
        controller.changeStarName(starName);


    }

    do {
        cout << "\nDo you want to change the star's mass ? y/n" << endl;
        cin >> resposta;
    } while (resposta != "y" && resposta != "n");

    if (resposta == "y") {
        cout << "\nInsert the star's new mass\n" << endl;
        cin >> starMass;
        controller.changeStarMass(starMass);

    }

    do {
        cout << "\nDo you want to change the solar System name ? y/n" << endl;
        cin >> resposta;
    } while (resposta != "y" && resposta != "n");

    if (resposta == "y") {
        cout << "\nInsert the solar system new name\n" << endl;
        cin >> solarSystemName;
        controller.changeSolarSystemName(solarSystemName);

    }

}

 bool EditSolarSystemUI::showBodiesAndAddOtherBody(OtherBody * ob) {
     int i = 0;
    list <Body *> bodys = controller.getBodys();
    vector < Body * > vetor_bodies;
    for (list<Body *>::iterator itr = bodys.begin(); itr != bodys.end(); ++itr) {
        if (typeid (*(*itr)) == typeid (Moon) || typeid (*(*itr)) == typeid (Planet))
            cout << i << "-" << (*itr)->getName() << endl;

        Body * b = *itr;
        vetor_bodies.push_back(b);

        i++;


    }
    cout << endl;


    int escolha;
    cout << "Choose the number of the body\n";
    cin >> escolha;

    ((Planet *) vetor_bodies[escolha ])->addOtherBody(ob);

    return true;
 }
 
bool EditSolarSystemUI::showBodiesAndAddMoon(Moon * m) {
    int i = 0;
    list <Body *> bodys = controller.getBodys();
    vector < Body * > vetor_bodies;
    for (list<Body *>::iterator itr = bodys.begin(); itr != bodys.end(); ++itr) {
        if (typeid (*(*itr)) == typeid (Moon) || typeid (*(*itr)) == typeid (Planet))
            cout << i << "-" << (*itr)->getName() << endl;

        Body * b = *itr;
        vetor_bodies.push_back(b);

        i++;


    }
    cout << endl;


    int escolha;
    cout << "Choose the number of the Planet\n";
    cin >> escolha;

    ((Planet *) vetor_bodies[escolha])->addMoons(m);

    return true;
}

bool EditSolarSystemUI::showSolarSystems() {
    int i = 1;
	list < SolarSystem * > sistemas;
	if (this->sistema->getDatabaseConnection()){
		sistemas = this->sistema->getDatabase()->getSolarSystems();
	}
	else{
		sistemas = this->sistema->getSolarSystems();
	}
   
    vector < SolarSystem * > vetor_sistemas;
    if (sistemas.size() != 0) {
        cout << "Solar systems" << endl;
        for (list<SolarSystem *>::iterator itr = sistemas.begin(); itr != sistemas.end(); ++itr) {
            cout << i << "-" << (*itr)->getName() << endl;

            SolarSystem * ss = *itr;
            vetor_sistemas.push_back(ss);

            i++;
        }
        cout << endl;

        int escolha;
        cout << "Choose the number of the solar system you want to edit:\n";
        cin >> escolha;

        controller.editSolarSystem(vetor_sistemas[escolha - 1]);
        return true;
    } else {

        cout << "There are no Solar Systems to edit..." << endl;
        cout << "Press any key to continue..." << endl;
        char s;
        cin >> s;
        return false;

    }
}

void EditSolarSystemUI::mostraNovoSistema() {

}

void EditSolarSystemUI::run() {
    if (showSolarSystems()) {
        if (addObjectsQuestion()) {
            addObjects();
        
        } else {
            if (editSolarSystemQuestion()) {
                editSolarSystem();
            } else {

            }
        }
    }
}




#endif	/* EDITSOLARSYSTEMUI_H */

