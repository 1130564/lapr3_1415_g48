/*
*
* @author Vitor Moreira <1130564@isep.ipp.pt>
* @author Paulo Andrade <1130313@isep.ipp.pt>
* @author Emanuel Marques <1130553@isep.ipp.pt>
* @author Ivo Joao <1130404@isep.ipp.pt>
* @author Aria Jozi <1130777@isep.ipp.pt>
* @version 1.0
*
*/

#ifndef BODY_H
#define	BODY_H
#include <string>
#include <vector>
#include "Calculo.h"

using namespace std;

class Body {
private:

    string name;
    double mass;
    vetor initialVelocity;
    vetor initialPosition;
	int id;
	

public:

    Body();
	Body(int id1, string name, double mass, vetor initialVelocity, vetor initialPosition);
    Body(string name, double mass, vetor initialVelocity, vetor initialPosition);
    Body(const Body& toCopy);
    
    virtual ~Body();
    virtual Body * clone() const = 0;
    
    string getName() const;
    double getMass() const;
    vetor getInitialVelocity() const;
    vetor getInitialPosition() const;
	int getID() const;
    
    void setName(string name);
    void setMass(double mass);
    void setInitialPosition(vetor initialPosition);
    void setInitialVelocity(vetor initialVelocity);
    
    virtual void escreve(ostream& ostr) const;

};

Body::Body() {
	this->id = newBody();
    this->name = "not defined";
}
Body::Body(int id1, string name, double mass, vetor initialVelocity, vetor initialPosition) {
	this->id = id1;
	this->name = name;
	this->mass = mass;
	this->initialPosition = initialPosition;
	this->initialVelocity = initialVelocity;
}

Body::Body(string name, double mass, vetor initialVelocity, vetor initialPosition) {
	this->id = newBody();
    this->name = name;
    this->mass = mass;
    this->initialPosition = initialPosition;
    this->initialVelocity = initialVelocity;
}

Body::Body(const Body& toCopy) {
	this->id = newBody();
    this->name = toCopy.name;
    this->mass = toCopy.mass;
    this->initialPosition = toCopy.initialPosition;
    this->initialVelocity = toCopy.initialVelocity;
}

Body::~Body() {

}

void Body::setName(string name) {
    this->name = name;
}

string Body::getName() const {
    return name;
}

void Body::setInitialPosition(vetor initialPosition) {
    this->initialPosition = initialPosition;
}

vetor Body::getInitialPosition() const {
    return initialPosition;
}

void Body::setInitialVelocity(vetor initialVelocity) {
    this->initialVelocity = initialVelocity;
}

vetor Body::getInitialVelocity() const {
    return initialVelocity;
}

void Body::setMass(double mass) {
    this->mass = mass;
}

double Body::getMass() const {
    return mass;
}
int Body::getID()const {
	return this->id;
}

void Body::escreve(ostream& ostr) const {
    ostr << "\nName: " << this->name << endl;
    ostr << "Mass: " << this->mass << endl;
    ostr << "Position x: " << this->initialPosition.x << endl;
    ostr << "Position y: " << this->initialPosition.y << endl;
    ostr << "Position z: " << this->initialPosition.z << endl;
    ostr << "Velocity x: " << this->initialVelocity.x << endl;
    ostr << "Velocity y: " << this->initialVelocity.y << endl;
    ostr << "Velocity z: " << this->initialVelocity.z << endl;
}

ostream& operator<<(ostream& ostr, const Body &b) {
    b.escreve(ostr);
    return ostr;
}





#endif	/* BODY_H */

