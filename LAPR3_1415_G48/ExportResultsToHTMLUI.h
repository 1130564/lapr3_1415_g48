/*
*
* @author Vitor Moreira <1130564@isep.ipp.pt>
* @author Paulo Andrade <1130313@isep.ipp.pt>
* @author Emanuel Marques <1130553@isep.ipp.pt>
* @author Ivo Joao <1130404@isep.ipp.pt>
* @author Aria Jozi <1130777@isep.ipp.pt>
* @version 1.0
*
*/

#ifndef EXPORTRESULTSTOHTMLUI_H
#define	EXPORTRESULTSTOHTMLUI_H

#include "ExportResultsToHTMLController.h"

class ExportResultsToHTMLUI {
public:
    ExportResultsToHTMLUI(System * sistema);
    ~ExportResultsToHTMLUI();

    void run();
    bool pedeConfirmacao() const;

private:
    System * sistema;
    ExportResultsToHTMLController controller;

};

ExportResultsToHTMLUI::ExportResultsToHTMLUI(System * sistema) : controller(sistema) {
    this->sistema = sistema;
}

ExportResultsToHTMLUI::~ExportResultsToHTMLUI() {
}

bool ExportResultsToHTMLUI::pedeConfirmacao() const {

    string resposta;
    do {
        cout << "\nSimulation: " << this->controller.getSimulation()->getName() << endl;
        cout << "File name: " << this->controller.getFileName() << endl;
        cout << "\nDo you want to export the results of this simulation to this file? y/n " << endl;
        cin >> resposta;

    } while (resposta != "y" && resposta != "n");

    if (resposta == "y") {
        return true;
    } else {
        return false;
    }

}

void ExportResultsToHTMLUI::run() {
    
    list<Simulation*> simulations = this->controller.getSimulationsList();
    vector<Simulation*> temp;
    int i = 1;
    int resposta;
    for(list<Simulation*>::iterator itr = simulations.begin(); itr != simulations.end(); ++itr) {
        temp.push_back(*itr);
        cout << i << ": " << (*itr)->getName() << endl;
    }
    
    do {
        cout << "\nChoose the number of the simulation: " << endl;
        cin >> resposta;

    } while (resposta < 0 && resposta > simulations.size());
    
    this->controller.setSimulation(temp[resposta-1]);
    
    string fileName;
    cout << "\nWrite the name of the file with html extension: " << endl;
    cin >> fileName;
    this->controller.setFileName(fileName);
    
    if(this->pedeConfirmacao()) {
        this->controller.exportGIF();
        this->controller.exportResults();
        cout << "Sucess!" << endl;
		char s = getchar();
    } else {
        cout << "Export of results to HTML canceled!" << endl;
    }

}

#endif	/* EXPORTRESULTSTOHTMLUI_H */

