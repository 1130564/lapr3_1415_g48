/* Copyright (c) 2010,11 NFM-ACS-DEI/ISEP :-)*/
/* badados.h */
/*
*
* @author Vitor Moreira <1130564@isep.ipp.pt>
* @author Paulo Andrade <1130313@isep.ipp.pt>
* @author Emanuel Marques <1130553@isep.ipp.pt>
* @author Ivo Joao <1130404@isep.ipp.pt>
* @author Aria Jozi <1130777@isep.ipp.pt>
* @version 1.0
*
*/
#ifndef BDados_
#define BDados_

#include <iomanip>
#include <occi.h>
#include "NaturalSatellite.h"
#include "Planet.h"

using namespace oracle::occi;

class BDados
{
private:
	Environment *env;
	Connection *ligacao;
	Statement *instrucao;
public:
	BDados(string user, string passwd, string db);
	~BDados();

	void pushSolarSystem(SolarSystem* ss);
	void pushStar(Star* star, int idSolarSystem);
	void pushBody(Body* body, int idSolarSystem);
	void pushOtherBody(OtherBody* ob, int idSolarSystem, int idPlaneta);
	void pushNaturalSattelite(NaturalSatellite* ns, int idSolarSystem);
	void pushPlanet(Planet* planet, int idSolarSystem);
	void pushSimulation(Simulation* simul);
	void pushPosition(int idBody, int timestep, float x, float y, float z, int idSimulation);
	void pushVelocity(int idBody, int timestep, float x, float y, float z, int idSimulation);
	void pushIDS(int simulationID, int starID, int bodyID, int solarSystemID);

	list <SolarSystem*> getSolarSystems();
	list <Simulation*> getSimulations();
	Star* getStarOfSolarSystem(int id);
	list <Planet*> getPlanetsStarOfSolarSystem(int id);
	void getIDS();
	Simulation* getSimulationByID(int id);
	SolarSystem* getSolarSystemBySimulationID(int id);
	void deleteSimulationVelPos(Simulation* s);
	void editSimulation(Simulation* s, string nome, int start, int end, int step);
	void editSolarSystem(SolarSystem* ss, string nome);
	void editStar(SolarSystem* ss, string nome, float massa);
	void deleteObjectsOfSolarSystem(SolarSystem* ss);
	bool deleteSimulation(int id);

};



BDados::BDados(string user, string passwd, string db)
{
	env = Environment::createEnvironment(Environment::DEFAULT);
	ligacao = env->createConnection(user, passwd, db);

}
BDados::~BDados()
{
	env->terminateConnection(ligacao);
	Environment::terminateEnvironment(env);
}

list <Simulation*> BDados::getSimulations(){
	list <Simulation* > simulations;
	instrucao = ligacao->createStatement("begin getSimulations(:1); end;");
	instrucao->registerOutParam(1, OCCICURSOR);
	instrucao->executeUpdate();
	ResultSet *rset = instrucao->getCursor(1);
	while (rset->next()){

		Simulation *ss = new Simulation(rset->getInt(1), rset->getString(2), rset->getInt(3), rset->getInt(4), rset->getInt(5));
		simulations.push_back(ss);


	}
	ligacao->terminateStatement(instrucao);
	return simulations;
}

Simulation* BDados::getSimulationByID(int id){

	instrucao = ligacao->createStatement("begin getSimulationByID(:1, :2); end;");
	instrucao->setInt(1, id);
	instrucao->registerOutParam(2, OCCICURSOR);
	instrucao->executeUpdate();
	ResultSet *rset = instrucao->getCursor(2);
	rset->next();
	Simulation *ss = new Simulation(rset->getInt(1), rset->getString(2), rset->getInt(3), rset->getInt(4), rset->getInt(5));

	ligacao->terminateStatement(instrucao);
	return ss;
}

SolarSystem* BDados::getSolarSystemBySimulationID(int id){

	instrucao = ligacao->createStatement("begin getSolarSystemBySimulationID(:1, :2); end;");
	instrucao->setInt(1, id);
	instrucao->registerOutParam(2, OCCICURSOR);
	instrucao->executeUpdate();
	ResultSet *rset = instrucao->getCursor(2);
	rset->next();
	SolarSystem *ss = new SolarSystem(rset->getInt(1), rset->getString(2));

	ligacao->terminateStatement(instrucao);
	return ss;
}

void BDados::deleteObjectsOfSolarSystem(SolarSystem* ss){
	instrucao = ligacao->createStatement("begin deleteObjectsOfSolarSystem(:1);end;");
	instrucao->setInt(1, ss->getID());
	ResultSet *rset = instrucao->executeQuery();
	ligacao->commit();
	instrucao->closeResultSet(rset);

}
bool BDados::deleteSimulation(int id){
	instrucao = ligacao->createStatement("begin deleteSimulation(:1);end;");
	instrucao->setInt(1, id);
	ResultSet *rset = instrucao->executeQuery();
	ligacao->commit();
	instrucao->closeResultSet(rset);
	return true;

}

void BDados::editSolarSystem(SolarSystem* ss, string nome){
	instrucao = ligacao->createStatement("begin editSolarSystem(:1,:2);end;");
	instrucao->setInt(1, ss->getID());
	instrucao->setString(2, nome);
	ResultSet *rset = instrucao->executeQuery();
	ligacao->commit();
	instrucao->closeResultSet(rset);
}

void BDados::deleteSimulationVelPos(Simulation* s){
	instrucao = ligacao->createStatement("begin deleteSimulationVelPos(:1);end;");
	instrucao->setInt(1, s->getID());
	ResultSet *rset = instrucao->executeQuery();
	ligacao->commit();
	instrucao->closeResultSet(rset);

}

void BDados::editSimulation(Simulation* s, string nome, int start, int end, int step){
	instrucao = ligacao->createStatement("begin editSimulation(:1,:2,:3,:4,:5);end;");
	instrucao->setInt(1, s->getID());
	instrucao->setString(2, nome);
	instrucao->setInt(3, start);
	instrucao->setInt(4, end);
	instrucao->setInt(5, step);
	ResultSet *rset = instrucao->executeQuery();
	ligacao->commit();
	instrucao->closeResultSet(rset);
}
void BDados::editStar(SolarSystem* ss, string nome, float massa){
	instrucao = ligacao->createStatement("begin editStar(:1, :2, :3);end;");
	instrucao->setInt(1, ss->getID());
	instrucao->setString(2, nome);
	instrucao->setFloat(3, massa);
	ResultSet *rset = instrucao->executeQuery();
	ligacao->commit();
	instrucao->closeResultSet(rset);
}
void BDados::pushSolarSystem(SolarSystem* ss){

	instrucao = ligacao->createStatement("begin insertSolarSystem(:1,:2);end;");
	instrucao->setInt(1, ss->getID());
	instrucao->setString(2, ss->getName());
	ResultSet *rset = instrucao->executeQuery();
	
	ligacao->commit();
	instrucao->closeResultSet(rset);
}

void BDados::pushStar(Star* star, int idSolarSystem){

	instrucao = ligacao->createStatement("begin insertStar(:1,:2,:3,:4);end;");
	instrucao->setInt(1, star->getID());
	instrucao->setString(2, star->getName());
	instrucao->setFloat(3, star->getMass());
	instrucao->setInt(4, idSolarSystem);
	ResultSet *rset = instrucao->executeQuery();
	ligacao->commit();
	instrucao->closeResultSet(rset);
}

void BDados::pushBody(Body* body, int idSolarSystem){

	instrucao = ligacao->createStatement("begin insertBody(:1,:2,:3,:4);end;");
	instrucao->setInt(1, body->getID());
	instrucao->setString(2, body->getName());
	instrucao->setDouble(3, body->getMass());
	instrucao->setInt(4, idSolarSystem);
	ResultSet *rset = instrucao->executeQuery();
	ligacao->commit();
	instrucao->closeResultSet(rset);
}

void BDados::pushOtherBody(OtherBody* body, int idSolarSystem, int idPlanet){
	pushBody(body, idSolarSystem);
	instrucao = ligacao->createStatement("begin insertOtherBody(:1,:2);end;");
	instrucao->setInt(1, body->getID());
	instrucao->setInt(2, idPlanet);
	instrucao->setInt(4, idSolarSystem);
	ResultSet *rset = instrucao->executeQuery();
	ligacao->commit();
	instrucao->closeResultSet(rset);
}

void BDados::pushNaturalSattelite(NaturalSatellite* ns, int idSolarSystem){
	pushBody(ns, idSolarSystem);
	instrucao = ligacao->createStatement("begin insertNaturalSattelite(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15);end;");
	instrucao->setInt(1, ns->getID());
	instrucao->setDouble(2, ns->getPeriodOfRevolution());
	instrucao->setDouble(3, ns->getOrbitalSpeed());
	instrucao->setDouble(4, ns->getInclOfAxisToOrbit());
	instrucao->setDouble(5, ns->getEquaDiameter());
	instrucao->setDouble(6, ns->getDensity());
	instrucao->setDouble(7, ns->getEscapeVelocity());
	instrucao->setDouble(8, ns->getSemimajorAxis());
	instrucao->setDouble(9, ns->getOrbitEccentricity());
	instrucao->setDouble(10, ns->getOrbitInclination());
	instrucao->setDouble(11, ns->getPerihelion());
	instrucao->setDouble(12, ns->getAphelion());
	instrucao->setDouble(13, ns->getMeanAnomaly());
	instrucao->setDouble(14, ns->getLongitudeOfPerihelion());
	instrucao->setDouble(15, ns->getLongitudeOfAscendingNode());

	ResultSet *rset = instrucao->executeQuery();
	ligacao->commit();
	instrucao->closeResultSet(rset);
}
void BDados::pushPlanet(Planet* planet, int idSolarSystem){
	pushNaturalSattelite(planet, idSolarSystem);
	instrucao = ligacao->createStatement("begin insertPlanet(:1);end;");
	instrucao->setInt(1, planet->getID());
	ResultSet *rset = instrucao->executeQuery();
	ligacao->commit();
	instrucao->closeResultSet(rset);
}

void BDados::pushSimulation(Simulation* simul){
	instrucao = ligacao->createStatement("begin insertSimulation(:1,:2,:3,:4,:5,:6);end;");
	instrucao->setInt(1, simul->getID());
	instrucao->setInt(2, simul->getSolarSystem()->getID());
	instrucao->setString(3, simul->getName());
	instrucao->setDouble(4, simul->getStart_time());
	instrucao->setDouble(5, simul->getEnd_time());
	instrucao->setDouble(6, simul->getTime_step());

	ResultSet *rset = instrucao->executeQuery();
	ligacao->commit();
	instrucao->closeResultSet(rset);
}
void BDados::pushVelocity(int idBody, int timestep, float x, float y, float z, int idSimulation){
	instrucao = ligacao->createStatement("begin insertVelocity(:1,:2,:3,:4,:5,:6);end;");
	instrucao->setInt(1, idBody);
	instrucao->setInt(2, timestep);
	instrucao->setFloat(3, x);
	instrucao->setFloat(4, y);
	instrucao->setFloat(5, z);
	instrucao->setInt(6, idSimulation);

	ResultSet *rset = instrucao->executeQuery();
	ligacao->commit();
	instrucao->closeResultSet(rset);
}

void BDados::pushPosition(int idBody, int timestep, float x, float y, float z, int idSimulation){
	instrucao = ligacao->createStatement("begin insertPosition(:1,:2,:3,:4,:5,:6);end;");
	instrucao->setInt(1, idBody);
	instrucao->setInt(2, timestep);
	instrucao->setFloat(3, x);
	instrucao->setFloat(4, y);
	instrucao->setFloat(5, z);
	instrucao->setInt(6, idSimulation);

	ResultSet *rset = instrucao->executeQuery();
	ligacao->commit();
	instrucao->closeResultSet(rset);
}


void BDados::pushIDS(int simulationID, int starID, int bodyID, int solarSystemID){

	instrucao = ligacao->createStatement("begin setIDS(:1,:2,:3,:4);end;");
	instrucao->setInt(1, simulationID);
	instrucao->setInt(2, starID);
	instrucao->setInt(3, bodyID);
	instrucao->setInt(4,solarSystemID);
	ResultSet *rset = instrucao->executeQuery();
	ligacao->commit();
	instrucao->closeResultSet(rset);
}


list <SolarSystem*> BDados::getSolarSystems()  {
	list <SolarSystem* > solarsystems;
	instrucao = ligacao->createStatement("begin getSolarSystems(:1); end;");
	instrucao->registerOutParam(1, OCCICURSOR);
	instrucao->executeUpdate();
	ResultSet *rset = instrucao->getCursor(1);
	while (rset->next()){

		SolarSystem *ss = new SolarSystem(rset->getInt(1), rset->getString(2));
		solarsystems.push_back(ss);


	}
	ligacao->terminateStatement(instrucao);
	return solarsystems;
}
Star* BDados::getStarOfSolarSystem(int id)  {
	Star* star;
	instrucao = ligacao->createStatement("begin getStarOfSolarSystem(:1, :2); end;");
	instrucao->setInt(1, id);
	instrucao->registerOutParam(2, OCCICURSOR);
	instrucao->executeUpdate();
	ResultSet *rset = instrucao->getCursor(2);
	rset->next();

	star = new Star(rset->getInt(1), rset->getString(2), rset->getFloat(3));

	
	ligacao->terminateStatement(instrucao);
	return star;
}

void BDados::getIDS()  {
	Star* star;
	instrucao = ligacao->createStatement("begin getIDS(:1); end;");
	instrucao->registerOutParam(1, OCCICURSOR);
	instrucao->executeUpdate();
	ResultSet *rset = instrucao->getCursor(1);
	rset->next();
	lastSimulationID = rset->getInt(1);
	lastStarID = rset->getInt(2);
	lastBodyID = rset->getInt(3);
	lastSolarSystemID = rset->getInt(4);

	ligacao->terminateStatement(instrucao);
}
list <Planet*>  BDados::getPlanetsStarOfSolarSystem(int id)  {
	list <Planet* > planets;
	instrucao = ligacao->createStatement("begin getPlanetsOfSolarSystem(:1, :2); end;");
	instrucao->setInt(1, id);
	instrucao->registerOutParam(2, OCCICURSOR);
	instrucao->executeUpdate();
	ResultSet *rset = instrucao->getCursor(2);
	cout << "teste" << endl;
	while (rset->next()){
		vetor velInicial;
		velInicial.x = rset->getDouble(4);
		velInicial.y = rset->getDouble(5);
		velInicial.z = rset->getDouble(6);
		vetor posInicial;
		posInicial.x = rset->getDouble(7);
		posInicial.y = rset->getDouble(8);
		posInicial.z = rset->getDouble(9);
		Planet *p = new Planet(rset->getInt(1), rset->getString(2), rset->getDouble(3),
			velInicial, posInicial, rset->getDouble(10),
			rset->getDouble(11), rset->getDouble(12),rset->getDouble(13),
			rset->getDouble(14), rset->getDouble(15), rset->getDouble(16),
			rset->getDouble(17), rset->getDouble(18), rset->getDouble(19), rset->getDouble(20), rset->getDouble(21), rset->getDouble(22), rset->getDouble(23));
		planets.push_back(p);


	}
	ligacao->terminateStatement(instrucao);
	return planets;
}



#endif
