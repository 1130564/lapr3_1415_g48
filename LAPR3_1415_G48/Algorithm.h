/* 
 * File:   Algorithm.h
 * Author: vitor_000
 *
 * Created on January 15, 2015, 4:45 PM
 */

#ifndef ALGORITHM_H
#define	ALGORITHM_H

#include <list>
#include "Body.h"
#include "Planet.h"
#include <typeinfo>


using namespace std;
using namespace consts;

void velocityVerlet(list<Body*> bodies, vector<vector<vetor> > &matrix, double starMass,
        double startTime, double endTime, double timeStep) {

    vector<vetor> velocitysToBeUpdated;
    vector<vetor> positionsToBeUpdated;
    vector<vetor> forces;


    int pos = 0;
    double time = startTime * 86400;
    double end_time = endTime * 86400;
    double time_step = timeStep * 86400;

    // bodies da simulaçao
    list<Body*> bodies_subset = bodies;

    while (time <= end_time) {

        for (list<Body*>::iterator itr = bodies_subset.begin(); itr != bodies_subset.end(); ++itr) {

            // limpar vector das forças
            forces.clear();

            // força em relaçao à estrela
            double m1 = (*itr)->getMass();
            double m2 = starMass;
            vetor p1 = (*itr)->getInitialPosition();
            vetor p2 = {0, 0, 0};
            vetor vecAux = newtonUniversalGravitation(m1, m2, p1, p2);
            forces.push_back(vecAux);

            // bodies da simulaçao
            list<Body*> bodies_subsetCopy = bodies;

            for (list<Body*>::iterator it = bodies_subsetCopy.begin(); it != bodies_subsetCopy.end(); ++it) {

                // comparacao ao proprio
                if (*it != *itr) {
                    double m1 = (*itr)->getMass();
                    double m2 = (*it)->getMass();
                    vetor p1 = (*itr)->getInitialPosition();
                    vetor p2 = (*it)->getInitialPosition();
                    vetor vecAux = newtonUniversalGravitation(m1, m2, p1, p2);
                    forces.push_back(vecAux);
                }

            }

            // calcular força resultante
            vetor resultantForce = calcResultantForce(forces);

            // calcular aceleraçao
            vetor acceleration = calcAcceleration(resultantForce, (*itr)->getMass());

            // calcular velocidade
            double velX = (*itr)->getInitialVelocity().x + (acceleration.x * time);
            double velY = (*itr)->getInitialVelocity().y + (acceleration.y * time);
            double velZ = (*itr)->getInitialVelocity().z + (acceleration.z * time);
            vetor velocity = {velX, velY, velZ};
            // colocar na matriz
            matrix[pos].push_back(velocity);

            // carregar para o vetor
            velocitysToBeUpdated.push_back(velocity);

            // calcular posicao
            double posX = (*itr)->getInitialPosition().x + (velocitysToBeUpdated[pos].x * time) - ((1 / 2) * acceleration.x * pow(time, 2));
            double posY = (*itr)->getInitialPosition().y + (velocitysToBeUpdated[pos].y * time) - ((1 / 2) * acceleration.y * pow(time, 2));
            double posZ = (*itr)->getInitialPosition().z + (velocitysToBeUpdated[pos].z * time) - ((1 / 2) * acceleration.z * pow(time, 2));
            vetor position = {posX, posY, posZ};
            // colocar na matriz
            matrix[pos].push_back(position);

            // carregar para o vetor
            positionsToBeUpdated.push_back(position);


            pos++;
        }

        // atualizar velocidades
        int c = 0;
        vetor velocidade;
        for (list<Body*>::iterator itrV = bodies_subset.begin(); itrV != bodies_subset.end(); ++itrV) {
            velocidade = velocitysToBeUpdated[c];
            (*itrV)->setInitialVelocity(velocidade);
            c++;
        }

        // atualizar posiçoes
        c = 0;
        vetor posicao;
        for (list<Body*>::iterator itrV = bodies_subset.begin(); itrV != bodies_subset.end(); ++itrV) {
            posicao = positionsToBeUpdated[c];
            (*itrV)->setInitialPosition(posicao);
            c++;
        }

        // limpar vetores
        velocitysToBeUpdated.clear();
        positionsToBeUpdated.clear();

        // proximo time_step
        time += time_step;

        // resetar posicao da matriz
        pos = 0;
    }
}

void kepler(list<Body*> bodies, vector<vector<vetor> > &matrix, double startTime, double endTime, double timeStep, double starMass) {

    list<Body*> simBodies = bodies;
    vector<double> meanAnomalys;
    int c = 0;
    int pos = 1;

    // preencher meanAnomalys
    for (list<Body*>::iterator itr = simBodies.begin(); itr != simBodies.end(); ++itr) {

        if ((typeid (*(*itr)).name()) == (typeid (Planet).name())) {
            Planet* p = dynamic_cast<Planet*> (*itr);

            meanAnomalys.push_back(p->getMeanAnomaly());

        } else if (typeid (*(*itr)).name() == typeid (Moon).name()) {
            Moon* m = dynamic_cast<Moon*> (*itr);

            meanAnomalys.push_back(m->getMeanAnomaly());
        }
    }

    double time = startTime * 86400;
    double end_time = endTime * 86400;
    double time_step = timeStep * 86400;

    // calc
    while (time <= end_time) {

        for (list<Body*>::iterator itr = simBodies.begin(); itr != simBodies.end(); ++itr) {

            if ((typeid (*(*itr)).name()) == (typeid (Planet).name())) {
                Planet* p = dynamic_cast<Planet*> (*itr);

                double e = p->getOrbitEccentricity();
                // with convertion of  km to AU
                double a = p->getSemimajorAxis() * 6.68458712E-9;
                double w = p->getLongitudeOfPerihelion();
                double omega = p->getLongitudeOfAscendingNode();
                double inclination = p->getOrbitInclination();

                // calc meanAnomalys
                for (list<Body*>::iterator it = simBodies.begin(); it != simBodies.end(); ++it) {
                    meanAnomalys[c] += ((sqrt((G * (starMass + (*it)->getMass())) / pow(a, 3))) * time_step);

                    if ((*itr) != (*it)) {
                        meanAnomalys[c] += ((sqrt((G * ((*itr)->getMass() + (*it)->getMass())) / pow(a, 3))) * time_step);
                    }
                }

                // calc eccentricAnomaly
                double E = eccentricAnomaly(meanAnomalys[c], e);

                // calc coordinates
                vetor coordinates = heliocentricCoordinates(a, E, e);
                vetor newCoordinates = J2000EclipticPlane(coordinates, w, omega, inclination);


                // atualizar matriz
                matrix[c][pos].x = newCoordinates.x;
                matrix[c][pos].y = newCoordinates.y;
                matrix[c][pos].z = newCoordinates.z;


            } else if (typeid (*(*itr)).name() == typeid (Moon).name()) {
                Moon* m = dynamic_cast<Moon*> (*itr);

                double e = m->getOrbitEccentricity();
                double a = m->getSemimajorAxis();
                double w = m->getLongitudeOfPerihelion();
                double omega = m->getLongitudeOfAscendingNode();
                double inclination = m->getOrbitInclination();

                // calc meanAnomalys
                for (list<Body*>::iterator it = simBodies.begin(); it != simBodies.end(); ++it) {
                    meanAnomalys[c] += ((sqrt((G * (starMass + (*it)->getMass())) / pow(a, 3))) * time_step);
                    if ((*itr) != (*it)) {
                        meanAnomalys[c] += ((sqrt((G * ((*itr)->getMass() + (*it)->getMass())) / pow(a, 3))) * time_step);
                    }
                }

                // calc eccentricAnomaly
                double E = eccentricAnomaly(meanAnomalys[c], e);

                // calc coordinates
                vetor coordinates = heliocentricCoordinates(a, E, e);
                vetor newCoordinates = J2000EclipticPlane(coordinates, w, omega, inclination);

                // atualizar matriz
                matrix[c].push_back(newCoordinates);
            }

            c++;

        }

        c = 0;
        pos += 2;
        time += time_step;
    }
}

void calcMatrix(list<Body*> bodies, vector<vector<vetor> >& matrix, double starMass,
        double startTime, double endTime, double timeStep) {

    velocityVerlet(bodies, matrix, starMass, startTime, endTime, timeStep);
    kepler(bodies, matrix, startTime, endTime, timeStep, starMass);

}
#endif	/* ALGORITHM_H */

