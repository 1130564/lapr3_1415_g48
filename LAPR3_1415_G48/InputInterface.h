/*
*
* @author Vitor Moreira <1130564@isep.ipp.pt>
* @author Paulo Andrade <1130313@isep.ipp.pt>
* @author Emanuel Marques <1130553@isep.ipp.pt>
* @author Ivo Joao <1130404@isep.ipp.pt>
* @author Aria Jozi <1130777@isep.ipp.pt>
* @version 1.0
*
*/

#ifndef INPUTINTERFACE_H
#define	INPUTINTERFACE_H

#include <string>
#include <string.h>
#include <vector>

#include "SolarSystem.h"

class InputInterface {
public:
    InputInterface();
    virtual ~InputInterface();
    virtual list <Body * > read(string file)  = 0;
private:


};

InputInterface::InputInterface() {

}

InputInterface::~InputInterface() {

}


#endif	/* INPUTINTERFACE_H */

