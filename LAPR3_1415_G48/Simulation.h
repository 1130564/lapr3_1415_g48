/*
*
* @author Vitor Moreira <1130564@isep.ipp.pt>
* @author Paulo Andrade <1130313@isep.ipp.pt>
* @author Emanuel Marques <1130553@isep.ipp.pt>
* @author Ivo Joao <1130404@isep.ipp.pt>
* @author Aria Jozi <1130777@isep.ipp.pt>
* @version 1.0
*
*/

#ifndef SIMULATION_H
#define	SIMULATION_H

#include <list>
#include <ctime>
#include <math.h>
#include <typeinfo>

using namespace std;

#include "Body.h"
#include "Calculo.h"
#include "SolarSystem.h"
#include "Algorithm.h"

class Simulation {
private:

    string name;
    double start_time;
    double end_time;
    double time_step;
	int ID;

    list < Body * > bodies_subset;
    SolarSystem* solarSystem;
    vector < vector < vetor > > velocitysPositions;

public:

    Simulation();
    Simulation(string name, list< Body * > bodies_subset, double start_time, double end_time, double time_step);
	Simulation(int id, string name, double start_time, double end_time, double time_step);
    ~Simulation();


    string getName() const;
    double getStart_time() const;
    double getEnd_time() const;
    double getTime_step() const;
    list<Body*> getBodies_subset() const;
    SolarSystem* getSolarSystem() const;
    vector<vector<vetor> > getVelocitysPositions() const;
	int getID();

    void setVelocitysPositions(vector<vector<vetor> > velocitysPositions);
    void setSolarSystem(SolarSystem* solarSystem);
    void setTime_step(double time_step);
    void setEnd_time(double end_time);
    void setStart_time(double start_time);
    void setBodies_subset(list<Body*> bodies_subset);
    void setName(string name);
	void setID(int id);


    void calcVelAndPos();
    void addBody(Body * body);
    void escreve(ostream& ostr) const;

};

Simulation::Simulation() : velocitysPositions(), bodies_subset() {
    SolarSystem st = SolarSystem();
    this->solarSystem = &st;
	this->ID = newSimulation();
}

Simulation::Simulation(int id, string name, double start_time, double end_time, double time_step) : velocitysPositions(), bodies_subset() {
	this->name = name;
	this->start_time = start_time;
	this->end_time = end_time;
	this->time_step = time_step;
	this->ID = id;

}

Simulation::Simulation(string name, list<Body*> bodies_subset, double start_time, double end_time, double time_step) : velocitysPositions(), bodies_subset() {
    this->name = name;
    this->bodies_subset = bodies_subset;
    this->start_time = start_time;
    this->end_time = end_time;
    this->time_step = time_step;
	this->ID = newSimulation();

}

Simulation::~Simulation() {

}

void Simulation::setVelocitysPositions(vector<vector<vetor> > velocitysPositions) {
    this->velocitysPositions = velocitysPositions;
}

vector<vector<vetor> > Simulation::getVelocitysPositions() const {
    return velocitysPositions;
}

void Simulation::setSolarSystem(SolarSystem* solarSystem) {
    this->solarSystem = solarSystem;
}

SolarSystem* Simulation::getSolarSystem() const {
    return this->solarSystem;
}

void Simulation::setTime_step(double time_step) {
    this->time_step = time_step;
}

double Simulation::getTime_step() const {
    return time_step;
}

void Simulation::setEnd_time(double end_time) {
    this->end_time = end_time;
}

double Simulation::getEnd_time() const {
    return end_time;
}

void Simulation::setStart_time(double start_time) {
    this->start_time = start_time;
}

double Simulation::getStart_time() const {
    return start_time;
}

void Simulation::setBodies_subset(list<Body*> bodies_subset) {
    this->bodies_subset = bodies_subset;
}

list<Body*> Simulation::getBodies_subset() const {
    return bodies_subset;
}

void Simulation::setName(string name) {
    this->name = name;
}

string Simulation::getName() const {
    return name;
}

void Simulation::addBody(Body * body) {
    this->bodies_subset.push_back(body);
}
void Simulation::calcVelAndPos() {

    // criar matriz
    list<Body*> bodies_subset = this->bodies_subset;
    for (list<Body*>::iterator itr = bodies_subset.begin(); itr != bodies_subset.end(); ++itr) {
        vector<vetor> newVec;
        this->velocitysPositions.push_back(newVec);
    }
    
    // preencher matriz
    calcMatrix(bodies_subset, this->velocitysPositions,
            this->solarSystem->getStar()->getMass(), this->start_time, this->end_time,
            this->time_step);
}

/*
 * Second method, not quite correct. (Only Newton's)
 * Not being used!
 */
//void Simulation::calcVelAndPos() {
//
//    list<Body*> bodysAux = this->bodies_subset;
//    cout << "teste x simulaion: " << bodysAux.front()->getInitialPosition().x << endl;
//    // preencher matriz
//    for (list<Body*>::iterator itr = bodysAux.begin(); itr != bodysAux.end(); ++itr) {
//        vector<vetor> newVec;
//        this->velocitysPositions.push_back(newVec);
//    }
//
//    vector<vetor> velocitysToBeUpdated;
//    vector<vetor> positionsToBeUpdated;
//    vector<vetor> forces;
//
//    int pos = 0;
//    double i = this->start_time;
//    while (i <= this->end_time) {
//
//        for (list<Body*>::iterator itr = this->bodies_subset.begin(); itr != this->bodies_subset.end(); ++itr) {
//
//            //if (typeid (*itr) == typeid (OtherBody)) {
//
//            forces.clear();
//
//            // força em relaçao à estrela
//            double m1 = (*itr)->getMass();
//            double m2 = this->solarSystem->getStar()->getMass();
//            vetor p1 = (*itr)->getInitialPosition();
//            vetor p2 = {0, 0, 0};
//            vetor vecAux = newtonUniversalGravitation(m1, m2, p1, p2);
//            forces.push_back(vecAux);
//            
//            list<Body*> bodysAux2 = this->bodies_subset;
//            for (list<Body*>::iterator it = bodysAux2.begin(); it != bodysAux2.end(); ++it) {
//
//                // comparar comparacao ao proprio
//                if (*it != *itr) {
//                    double m1 = (*itr)->getMass();
//                    double m2 = (*it)->getMass();
//                    vetor p1 = (*itr)->getInitialPosition();
//                    vetor p2 = (*it)->getInitialPosition();
//                    vetor vecAux = newtonUniversalGravitation(m1, m2, p1, p2);
//                    forces.push_back(vecAux);
//                }
//
//            }
//
//            // calcular força resultante
//            vetor resultantForce = calcResultantForce(forces);
//
//            // calcular aceleraçao
//            vetor acceleration = calcAcceleration(resultantForce, (*itr)->getMass());
//
//            // calcular velocidade
//            double velX = (*itr)->getInitialVelocity().x + (acceleration.x * i * 86400);
//            double velY = (*itr)->getInitialVelocity().y + (acceleration.y * i * 86400);
//            double velZ = (*itr)->getInitialVelocity().z + (acceleration.z * i * 86400);
//            vetor velocity = {velX, velY, velZ};
//            // colocar na matriz
//            this->velocitysPositions[pos].push_back(velocity);
//
//            // carregar para o vetor
//            velocitysToBeUpdated.push_back(velocity);
//
//            // calcular posicao
//            double posX = (*itr)->getInitialPosition().x + ((*itr)->getInitialVelocity().x * i * 86400) - ((1 / 2) * acceleration.x * pow(i * 86400, 2));
//            double posY = (*itr)->getInitialPosition().y + ((*itr)->getInitialVelocity().y * i * 86400) - ((1 / 2) * acceleration.y * pow(i * 86400, 2));
//            double posZ = (*itr)->getInitialPosition().z + ((*itr)->getInitialVelocity().z * i * 86400) - ((1 / 2) * acceleration.z * pow(i * 86400, 2));
//            vetor position = {posX, posY, posZ};
//            // colocar na matriz
//            this->velocitysPositions[pos].push_back(position);
//
//            // carregar para o vetor
//            positionsToBeUpdated.push_back(position);
//
//            //}
//            pos++;
//        }
//
//        // atualizar velocidades
//        int c = 0;
//        for (list<Body*>::iterator itrV = bodysAux.begin(); itrV != bodysAux.end(); ++itrV) {
//            (*itrV)->setInitialVelocity(velocitysToBeUpdated[c]);
//            c++;
//        }
//
//        // atualizar posiçoes
//        c = 0;
//        for (list<Body*>::iterator itrV = bodysAux.begin(); itrV != bodysAux.end(); ++itrV) {
//            (*itrV)->setInitialPosition(positionsToBeUpdated[c]);
//            c++;
//        }
//
//        // limpar vetores
//        velocitysToBeUpdated.clear();
//        positionsToBeUpdated.clear();
//
//        // proximo time_step
//        i = i + time_step;
//
//        // resetar posicao da matriz
//        pos = 0;
//    }
//
//}


/*
 * First method, not correct. (Only Newton's)
 * Not being used!
 */
//void Simulation::calcVelAndPos() {
//
//    list<Body*> bodysAux = this->solarSystem->getObjects();
//    cout << "teste x simulaion: " << bodysAux.front()->getInitialPosition().x << endl;
//    // preencher matriz
//    for (list<Body*>::iterator itr = bodysAux.begin(); itr != bodysAux.end(); ++itr) {
//        vector<vetor> newVec;
//        this->velocitysPositions.push_back(newVec);
//    }
//
//    vector<vetor> velocitysToBeUpdated;
//    vector<vetor> positionsToBeUpdated;
//    vector<vetor> forces;
//
//    int pos = 0;
//    double i = this->start_time;
//    while (i <= this->end_time) {
//
//        for (list<Body*>::iterator itr = bodysAux.begin(); itr != bodysAux.end(); ++itr) {
//
//            //if (typeid (*itr) == typeid (OtherBody)) {
//
//            forces.clear();
//
//            // força em relaçao à estrela
//            double m1 = (*itr)->getMass();
//            double m2 = this->solarSystem->getStar()->getMass();
//            vetor p1 = (*itr)->getInitialPosition();
//            vetor p2 = {0, 0, 0};
//            vetor vecAux = newtonUniversalGravitation(m1, m2, p1, p2);
//            forces.push_back(vecAux);
//            
//            list<Body*> bodysAux2 = this->solarSystem->getObjects();
//            for (list<Body*>::iterator it = bodysAux2.begin(); it != bodysAux2.end(); ++it) {
//
//                // comparar comparacao ao proprio
//                if (*it != *itr) {
//                    double m1 = (*itr)->getMass();
//                    double m2 = (*it)->getMass();
//                    vetor p1 = (*itr)->getInitialPosition();
//                    vetor p2 = (*it)->getInitialPosition();
//                    vetor vecAux = newtonUniversalGravitation(m1, m2, p1, p2);
//                    forces.push_back(vecAux);
//                }
//
//            }
//
//            // calcular força resultante
//            vetor resultantForce = calcResultantForce(forces);
//
//            // calcular aceleraçao
//            vetor acceleration = calcAcceleration(resultantForce, (*itr)->getMass());
//
//            // calcular velocidade
//            double velX = (*itr)->getInitialVelocity().x + (acceleration.x * i * 86400);
//            double velY = (*itr)->getInitialVelocity().y + (acceleration.y * i * 86400);
//            double velZ = (*itr)->getInitialVelocity().z + (acceleration.z * i * 86400);
//            vetor velocity = {velX, velY, velZ};
//            // colocar na matriz
//            this->velocitysPositions[pos].push_back(velocity);
//
//            // carregar para o vetor
//            velocitysToBeUpdated.push_back(velocity);
//
//            // calcular posicao
//            double posX = (*itr)->getInitialPosition().x + ((*itr)->getInitialVelocity().x * i * 86400) - ((1 / 2) * acceleration.x * pow(i * 86400, 2));
//            double posY = (*itr)->getInitialPosition().y + ((*itr)->getInitialVelocity().y * i * 86400) - ((1 / 2) * acceleration.y * pow(i * 86400, 2));
//            double posZ = (*itr)->getInitialPosition().z + ((*itr)->getInitialVelocity().z * i * 86400) - ((1 / 2) * acceleration.z * pow(i * 86400, 2));
//            vetor position = {posX, posY, posZ};
//            // colocar na matriz
//            this->velocitysPositions[pos].push_back(position);
//
//            // carregar para o vetor
//            positionsToBeUpdated.push_back(position);
//
//            //}
//            pos++;
//        }
//
//        // atualizar velocidades
//        int c = 0;
//        for (list<Body*>::iterator itrV = bodysAux.begin(); itrV != bodysAux.end(); ++itrV) {
//            (*itrV)->setInitialVelocity(velocitysToBeUpdated[c]);
//            c++;
//        }
//
//        // atualizar posiçoes
//        c = 0;
//        for (list<Body*>::iterator itrV = bodysAux.begin(); itrV != bodysAux.end(); ++itrV) {
//            (*itrV)->setInitialPosition(positionsToBeUpdated[c]);
//            c++;
//        }
//
//        // limpar vetores
//        velocitysToBeUpdated.clear();
//        positionsToBeUpdated.clear();
//
//        // proximo time_step
//        i = i + time_step;
//
//        // resetar posicao da matriz
//        pos = 0;
//    }
//
//}

void Simulation::setID(int id){
	this->ID = id;
}

int Simulation::getID(){
	return this->ID;
}

void Simulation::escreve(ostream& ostr) const {
    ostr << "Name: " << this->name << endl;
}

ostream& operator<<(ostream& ostr, const Simulation &s) {
    s.escreve(ostr);
    return ostr;
}
#endif	/* SIMULATION_H */

