/*
*
* @author Vitor Moreira <1130564@isep.ipp.pt>
* @author Paulo Andrade <1130313@isep.ipp.pt>
* @author Emanuel Marques <1130553@isep.ipp.pt>
* @author Ivo Joao <1130404@isep.ipp.pt>
* @author Aria Jozi <1130777@isep.ipp.pt>
* @version 1.0
*
*/

#ifndef DELETESIMULATIONCONTROLLER_H
#define	DELETESIMULATIONCONTROLLER_H

using namespace std;

#include "System.h"


class DeleteSimulationController {
public:
    DeleteSimulationController(System * sistema);
    ~DeleteSimulationController();

    list<Simulation*> getSimulationsList();
    bool removeSimulationByID(int id);
    Simulation* getSimulationByID(int id) const;
    void setSimulationName(string name);
    //string getSimulationName() const;
	void deleteFromDB(Simulation* s);
    
private:
    System * sistema;


};

DeleteSimulationController::DeleteSimulationController(System * sistema) {
    this->sistema = sistema;
}

DeleteSimulationController::~DeleteSimulationController() {
}

list<Simulation*> DeleteSimulationController::getSimulationsList() {
    return this->sistema->getSimulations();
}

bool DeleteSimulationController::removeSimulationByID(int id) {
	if (this->sistema->getDatabaseConnection()){
		return this->sistema->getDatabase()->deleteSimulation(id);
	}
	else{
		return this->sistema->removeSimulationByID(id);
	}
    
}

Simulation* DeleteSimulationController::getSimulationByID(int id) const {

	if (this->sistema->getDatabaseConnection()){
		return this->sistema->getDatabase()->getSimulationByID(id);
	}
	else{
		return this->sistema->returnsSimulationByID(id);
	}
}


void DeleteSimulationController::deleteFromDB(Simulation* s){
	if (this->sistema->getDatabaseConnection()){
		this->sistema->getDatabase()->deleteSimulation(s->getID());
	}
}



#endif	/* DELETESIMULATIONCONTROLLER_H */

