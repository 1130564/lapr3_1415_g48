/*
*
* @author Vitor Moreira <1130564@isep.ipp.pt>
* @author Paulo Andrade <1130313@isep.ipp.pt>
* @author Emanuel Marques <1130553@isep.ipp.pt>
* @author Ivo Joao <1130404@isep.ipp.pt>
* @author Aria Jozi <1130777@isep.ipp.pt>
* @version 1.0
*
*/

#ifndef OTHERBODY_H
#define	OTHERBODY_H

#include "Body.h"


using namespace std;

class OtherBody : public Body {
private:

public:

    OtherBody();
    OtherBody(string name, double mass, vetor initialVelocity, vetor initialPosition);
    OtherBody(const OtherBody& toCopy);

    ~OtherBody();
    OtherBody * clone() const;

    void escreve(ostream& ostr) const;
};

OtherBody::OtherBody() : Body() {

}

OtherBody::OtherBody(string name, double mass, vetor initialVelocity, vetor initialPosition) : Body(name, mass, initialVelocity, initialPosition) {

}

OtherBody::OtherBody(const OtherBody& toCopy) : Body(toCopy) {

}

OtherBody::~OtherBody() {

}

OtherBody* OtherBody::clone() const {
    return new OtherBody(*this);
}

void OtherBody::escreve(ostream& ostr) const {
    ostr << "Other Body" << endl;
    Body::escreve(ostr);
}

#endif	/* OTHERBODY_H */

