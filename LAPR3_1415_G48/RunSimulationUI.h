/*
*
* @author Vitor Moreira <1130564@isep.ipp.pt>
* @author Paulo Andrade <1130313@isep.ipp.pt>
* @author Emanuel Marques <1130553@isep.ipp.pt>
* @author Ivo Joao <1130404@isep.ipp.pt>
* @author Aria Jozi <1130777@isep.ipp.pt>
* @version 1.0
*
*/

#ifndef RUNSIMULATIONUI_H
#define	RUNSIMULATIONUI_H

#include "RunSimulationController.h"
#include "Simulation.h"
#include "CsvReader.h"
#include "HTMLWriter.h"

class RunSimulationUI {
public:
    RunSimulationUI(System * sistema);
    ~RunSimulationUI();

    void run();

private:
    System * sistema;
    RunSimulationController controller;

};

RunSimulationUI::RunSimulationUI(System * sistema) : controller(sistema) {
    this->sistema = sistema;
}

RunSimulationUI::~RunSimulationUI() {
}

void RunSimulationUI::run() {

    list<Simulation*> simulations;
    vector<Simulation*> temp;
    simulations = this->controller.getSimulationsList();
    int i = 1;
    int resposta = 0;

    for (list<Simulation*>::iterator itr = simulations.begin(); itr != simulations.end(); ++itr) {
        cout << i << "-" << *(*itr) << endl;
        temp.push_back(*itr);
        i++;
    }

    cout << "Choose the number of the simulation:" << endl;
    cin >> resposta;
    if (resposta > 0 && resposta <= simulations.size()) {
        controller.setSimulation(temp[resposta-1]);
    }

    // preencher matriz
    controller.runSimulation();
	controller.pushToDB();

    // exportar para csv
    ExportInterface * exp = new CsvWriter();
    exp->writeVelPosMatrix("matriz.csv", controller.getSimulation());

    //exportar html

    exp = new HTMLWriter();
    exp->writeVelPosMatrix("matriz.html", controller.getSimulation());
    
    cout << "Sucess!" << endl;
	cout << "Press any key to continue..." << endl;
	char s;
	cin >> s;
}

#endif	/* RUNSIMULATIONUI_H */

