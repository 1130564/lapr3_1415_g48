/*
*
* @author Vitor Moreira <1130564@isep.ipp.pt>
* @author Paulo Andrade <1130313@isep.ipp.pt>
* @author Emanuel Marques <1130553@isep.ipp.pt>
* @author Ivo Joao <1130404@isep.ipp.pt>
* @author Aria Jozi <1130777@isep.ipp.pt>
* @version 1.0
*
*/

#ifndef HTMLWRITER_H
#define HTMLWRITER_H
#include <iostream>
#include <fstream>
#include <string>

#include "Simulation.h"

using namespace std;


class HTMLWriter : public ExportInterface
{
public:
	HTMLWriter();
	~HTMLWriter();

	
	void writeVelPosMatrix(const string nome, Simulation * simulation);
	

private:
	void novoFicheiro(string nome);
	void fecharFicheiro(string nome);

	ofstream out;
};

HTMLWriter::HTMLWriter() : ExportInterface()
{
}

HTMLWriter::~HTMLWriter()
{
}

void HTMLWriter::novoFicheiro(string nome){
	
	out.open(nome+".html");

	out << "\n<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\"> "
		<< "\n <html> \n "
		<< "<head>\n<script type=\"text/javascript\" src=\"https://moodleant.isep.ipp.pt/lib/ufo.js\"></script> "
		<< "\n <link rel=\"stylesheet\" type=\"text/css\"  href=\"https://moodleant.isep.ipp.pt/theme/standard/styles.php\" > "
		<< "\n <link rel=\"stylesheet\" type=\"text/css\"  href=\"https://moodleant.isep.ipp.pt/theme/standard/styles.php\" > "
		<< "\n <link rel=\"stylesheet\" type=\"text/css\"  href=\"https://moodleant.isep.ipp.pt/theme/ISEP_Novo/styles.php\" > "
		<< "\n <title>" << nome << ".html</title>"
		<< "\n <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"> "
		<< "\n <link rel=\"icon\" type=\"image/ico\" href=\"http://www.isep.ipp.pt/favicon.ico\"> "
		<< "\n </head>"
		<< "\n<body>\n"<<endl;

	out << "<div> \n <center><img src=\"http://www.dei.isep.ipp.pt/images/topo_index.png\" alt=\"Logotipo ISEP\"></center> \n </div>\n<hr>\n "<<endl;
	out << "<div>\n";
}

void HTMLWriter::writeVelPosMatrix(const string nome, Simulation * simulation){
	novoFicheiro(nome);
	vector<vector<vetor> > matriz = simulation->getVelocitysPositions();
	list<Body *> objects = simulation->getBodies_subset();

	out << "<table> \n <tr>\n<td>";
	out << "<table border=1> \n";

	// escrever nomes dos planetas
	out << "<tr>\n<td></td>";
	for (list<Body *>::iterator itr = objects.begin(); itr != objects.end(); ++itr) {

		out << "<td>"<<(*(*itr)).getName()<<"</td>" <<endl;
	}
	out << "</tr>";
	//--

	// escrever velocidades e posiçoes a partir da matriz
	

	for (int i = 0; i < matriz[0].size(); i+=2) {
		//velocidades
		out<<"<tr>\n";
		out << "<td> Velocity </td>";
		for (vector<vector<vetor> >::iterator it = matriz.begin(); it != matriz.end(); ++it) {

			vector<vetor> aux = (*it);
			out << "<td>&nbsp " << aux[i].x << ", " << aux[i].y << ", " << aux[i].z << "&nbsp</td>\n";
		}		
		out<<"</tr>";
		//--

		//posiçoes
		out << "<tr>\n";
		out << "<td> Position </td>";
		for (vector<vector<vetor> >::iterator it = matriz.begin(); it != matriz.end(); ++it) {

			vector<vetor> aux = (*it);
			out << "<td>&nbsp " << aux[i+1].x << ", " << aux[i+1].y << ", " << aux[i+1].z << "&nbsp</td>\n";
		}
		out << "</tr> ";

	}
	
	out<< "</table> \n";
	out << "</table>";
	fecharFicheiro(nome);
}

void HTMLWriter::fecharFicheiro( string nome){
	out << "</div>";
	out << "<img src = " + nome + ".gif alt = Simulation>\n";
	out <<"<p>\n"
		<< "    <a href=\"http://validator.w3.org/check?uri=referer\"><img\n"
		<< "      src=\"http://www.w3.org/Icons/valid-html401\" alt=\"Valid HTML 4.01 Transitional\" height=\"31\" width=\"88\"></a>\n"
		<< "  </p>";

	out << "</body>\n</html>";
	out.close();


}
#endif