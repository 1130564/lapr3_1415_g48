/*
*
* @author Vitor Moreira <1130564@isep.ipp.pt>
* @author Paulo Andrade <1130313@isep.ipp.pt>
* @author Emanuel Marques <1130553@isep.ipp.pt>
* @author Ivo Joao <1130404@isep.ipp.pt>
* @author Aria Jozi <1130777@isep.ipp.pt>
* @version 1.0
*
*/

#ifndef CALCULO_H
#define	CALCULO_H

using namespace std;

#include <math.h>
#include <vector>

namespace consts {
    /**
     * to be used when needed
     */

    // 6.67384 × 10-11 m3 kg-1 s-2
    double G = 6.67384E-11;
}

using namespace consts;

// struct to be used as a vector

typedef struct {
    double x;
    double y;
    double z;
} vetor;

double distanceBetweenTwoPoints(vetor v1, vetor v2) {
    double c2 = pow(v1.x - v2.x, 2) + pow(v1.y - v2.y, 2) + pow(v1.z - v2.z, 2);
    return sqrt(c2);
}

vetor calcAcceleration(vetor resultantForce, double mass) {
    vetor a;
    a.x = resultantForce.x / mass;
    a.y = resultantForce.y / mass;
    a.z = resultantForce.z / mass;

    return a;
}

vetor calcResultantForce(vector<vetor> all_forces) {
    vetor resultantForce{0, 0, 0};
    for (vector<vetor>::iterator itr = all_forces.begin(); itr != all_forces.end(); ++itr) {
        resultantForce.x += (*itr).x;
        resultantForce.y += (*itr).y;
        resultantForce.z += (*itr).z;
    }

    return resultantForce;
}

vetor vectorBetweenTwoPoints(vetor v1, vetor v2) {
    vetor v;
    v.x = v1.x - v2.x;
    v.y = v1.y - v2.y;
    v.z = v1.z - v2.z;
    return v;
}

vetor newtonUniversalGravitation(double m1, double m2, vetor p1, vetor p2) {

    vetor force;

    double distance = distanceBetweenTwoPoints(p1, p2);
    if (distance == 0) {
        force = {0, 0, 0};
        return force;
    }

    vetor vr = vectorBetweenTwoPoints(p1, p2);

    double f = -G * ((m1 * m2) / pow(distance, 2));

    force.x = f * (vr.x / distance);
    force.y = f * (vr.y / distance);
    force.z = f * (vr.z / distance);

    return force;

}

/**
 * Retrieved from Moodle
 * https://moodle.isep.ipp.pt/pluginfile.php/10842/course/section/2628/Kepler_v02.pdf
 * @param M
 * @param eccentricity
 * @return 
 */
double eccentricAnomaly(double M, double eccentricity) {
    double E = M + eccentricity * sin(M);
    double de = 0;

    do {
        de = (M - (E - eccentricity * sin(E))) / (1 - eccentricity * cos(E));
        E = E + de;
    } while (de > 0.000001); //	10E-06	is	an	acceptable	value

    return E;
}

vetor heliocentricCoordinates(double a, double E, double e) {
    vetor coordinates = {0, 0, 0};
    coordinates.x = a * (cos(E) - e);
    coordinates.y = a * sqrt(1 - pow(e, 2)) * sin(E);
    // z is 0

    return coordinates;
}

vetor J2000EclipticPlane(vetor coordinates, double w, double omega, double inclination) {
    vetor newCoordinates = {0, 0, 0};
    newCoordinates.x = (cos(w) * cos(omega) - sin(w) * sin(omega) * cos(inclination)) * coordinates.x + (-sin(w) * cos(omega) - cos(w) * sin(omega) * cos(inclination)) * coordinates.y;
    newCoordinates.y = (cos(w) * sin(omega) + sin(w) * cos(omega) * cos(inclination)) * coordinates.x + (-sin(w) * sin(omega) + cos(w) * cos(omega) * cos(inclination)) * coordinates.y;
    newCoordinates.z = (sin(w) * sin(inclination)) * coordinates.x + (cos(w) * sin(inclination)) * coordinates.y;

    return newCoordinates;
}

#endif	/* CALCULO_H */