/*
*
* @author Vitor Moreira <1130564@isep.ipp.pt>
* @author Paulo Andrade <1130313@isep.ipp.pt>
* @author Emanuel Marques <1130553@isep.ipp.pt>
* @author Ivo Joao <1130404@isep.ipp.pt>
* @author Aria Jozi <1130777@isep.ipp.pt>
* @version 1.0
*
*/

#ifndef COPYSIMULATIONUI_H
#define	COPYSIMULATIONUI_H

#include "CopySimulationController.h"
#include "EditSimulationUI.h"

class CopySimulationUI {
private:

    System *sistema;
    CopySimulationController controller;

public:

    CopySimulationUI (System * sistema);
    ~CopySimulationUI();

    void mostraSimulacoes();
    void mostraNovaSimulacao();
    bool pedeConfirmacao();

    void run();
};

CopySimulationUI::CopySimulationUI(System * sistema) : controller(sistema) {
    this->sistema = sistema;
}

CopySimulationUI::~CopySimulationUI() {
   
}

void CopySimulationUI::mostraSimulacoes() {
    int i = 1;
	list < Simulation * > simulacoes;
	if (this->sistema->getDatabaseConnection()){
		simulacoes = this->sistema->getDatabase()->getSimulations();
	}
	else{
		simulacoes = this->sistema->getSimulations();
	}
    vector < Simulation * > vetor_simulacoes;
    cout << "Simulations"<<endl;
    for ( list< Simulation * >::iterator itr = simulacoes.begin(); itr != simulacoes.end(); ++itr) {
        cout << i << "-" << (*itr)->getName() << endl;
        Simulation * s = *itr;
        vetor_simulacoes.push_back(s);
        i++;
    }
    cout << endl;
    
    int escolha;
    cout << "Choose the number of the simulation you want to copy from:";
    cin >> escolha;
    controller.createNewSimulation(vetor_simulacoes[escolha - 1]);
}

bool CopySimulationUI::pedeConfirmacao() {
    string resposta;
    do {
        cout << "\nDo you confirm the creation of the simulation? y/n" << endl;
        cin >> resposta;
    } while (resposta != "y" && resposta != "n");

    if (resposta == "y") {
        return true;
    } else {
        return false;
    }

}

void CopySimulationUI::mostraNovaSimulacao() {
    controller.mostraNovaSimulacao();
}

void CopySimulationUI::run() {
	list < Simulation * > simulations;
	if (this->sistema->getDatabaseConnection()){
		simulations = this->sistema->getDatabase()->getSimulations();
	}
	else{
		simulations = this->sistema->getSimulations();
	}
    if(simulations.empty()){
        cout << "No simulations available."<<endl;
		cout << "Press any key to continue..." << endl;
		char t;
		cin >> t;
    }
    else{
    mostraSimulacoes();
    mostraNovaSimulacao();
    controller.alterarDados();
    
    if (pedeConfirmacao()) {
        controller.confirma();
        cout << "Success" << endl;
		cout << "Press any key to continue..." << endl;
		char t;
		cin >> t;
    } else {
        cout << "Operation canceled" << endl;
		cout << "Press any key to continue..." << endl;
		char t;
		cin >> t;
    }
    }
}


#endif	/* COPYSIMULATIONUI_H */

