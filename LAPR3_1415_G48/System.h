/*
*
* @author Vitor Moreira <1130564@isep.ipp.pt>
* @author Paulo Andrade <1130313@isep.ipp.pt>
* @author Emanuel Marques <1130553@isep.ipp.pt>
* @author Ivo Joao <1130404@isep.ipp.pt>
* @author Aria Jozi <1130777@isep.ipp.pt>
* @version 1.0
*
*/

#ifndef SYSTEM_H
#define	SYSTEM_H

using namespace std;
#include "Simulation.h"
#include "SolarSystem.h"
#include "BDados.h"



class System {
private:

    //list of simulations
    list < Simulation * > simulations_list;
    //list of solar systems
    list < SolarSystem * > solarSystems_list;
	//true if connected to database. false otherwise
	bool databaseConnection;
	BDados* database;


public:
    System();
    System(list <Simulation * > simulation_list, list < SolarSystem * > solar_system);
    ~System();

    list<Simulation* > getSimulations() const;
    list<SolarSystem* > getSolarSystems() const;
	

	void setDatabase(BDados* db);
	BDados* getDatabase() const;
	bool getDatabaseConnection();

    void setSimulation(list<Simulation*> simul);
    void setSolarSystem(list<SolarSystem*> sSyst);

	void setDatabaseConnection(bool setter);

    bool addSimulation(Simulation* simul);
    void addSolarSystem(SolarSystem* ss);
    bool removeSolarSytemByStarName(string name);
    bool removeSimulationByID(int id);
    Simulation* returnsSimulationByID(int id);
    SolarSystem* returnsSolarSystemByStarName(string name);
};

System::System() : simulations_list(), solarSystems_list() {
	resetIDS();

}

System::System(list<Simulation*> simulations_list, list<SolarSystem*> solarSystems_list) {

	resetIDS();
    this->simulations_list = simulations_list;
    this->solarSystems_list = solarSystems_list;
}

System::~System() {

}

list<Simulation*> System::getSimulations() const {
    return this->simulations_list;
}

void System::setSimulation(list<Simulation*> simul) {
    this->simulations_list = simul;

}

list <SolarSystem*> System::getSolarSystems() const {
    return this->solarSystems_list;
}

void System::setSolarSystem(list<SolarSystem*> sSyst) {
    this->solarSystems_list = sSyst;
}

bool System::addSimulation(Simulation* s) {
    this->simulations_list.push_back(s);
    return true;
}

void System::addSolarSystem(SolarSystem* ss) {
    this->solarSystems_list.push_back(ss);
}



//removes a solar system by a star name

bool System::removeSolarSytemByStarName(string name) {

    list<SolarSystem*>::iterator itr;
    for (itr = this->solarSystems_list.begin(); itr != this->solarSystems_list.end(); ++itr) {
        if ((*itr)->getStar()->getName() == name) {
            this->solarSystems_list.remove((*itr));
            return true;
        }
    }
    return false;
}

//removes a simulation by its ID
bool System::removeSimulationByID(int id) {

    list<Simulation*>::iterator itr;
    for (itr = this->simulations_list.begin(); itr != this->simulations_list.end(); ++itr) {
        if ((*itr)->getID() == id) {
            this->simulations_list.remove((*itr));
            return true;
        }
    }
    return false;
}

// returns a Simulation giving its name

Simulation* System::returnsSimulationByID(int id) {

    list<Simulation*>::iterator itr;
    for (itr = this->simulations_list.begin(); itr != this->simulations_list.end(); ++itr) {
        if ((*itr)->getID() == id) {
            return *itr;
        }
    }
	return NULL;

}

// returns a Simulation giving its name

SolarSystem* System::returnsSolarSystemByStarName(string name) {

    list<SolarSystem*>::iterator itr;
    for (itr = this->solarSystems_list.begin(); itr != this->solarSystems_list.end(); ++itr) {
        if ((*itr)->getStar()->getName() == name) {
            return *itr;
        }
    }
	return NULL;
}
void System::setDatabaseConnection(bool setter){
	this->databaseConnection = setter;
}


bool System::getDatabaseConnection(){
	return this->databaseConnection;
}
BDados* System::getDatabase() const{
	return this->database;
}

void System::setDatabase(BDados* db){
	this->database = db;
}


#endif	/* SYSTEM_H */
