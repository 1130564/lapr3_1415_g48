/*
*
* @author Vitor Moreira <1130564@isep.ipp.pt>
* @author Paulo Andrade <1130313@isep.ipp.pt>
* @author Emanuel Marques <1130553@isep.ipp.pt>
* @author Ivo Joao <1130404@isep.ipp.pt>
* @author Aria Jozi <1130777@isep.ipp.pt>
* @version 1.0
*
*/

#ifndef OPENSOLARSYSTEMUI_H
#define	OPENSOLARSYSTEMUI_H

#include "System.h"
#include "OpenSolarSystemController.h"

using namespace std;

class OpenSolarSystemUI {
public:
    OpenSolarSystemUI();
    OpenSolarSystemUI(System* sistema);
    virtual ~OpenSolarSystemUI();
	void open(const SolarSystem* sistema);
    void run();
    void mostrarSistemas();
private:

	OpenSolarSystemController controller;
	System* sistema;
        
	
};
OpenSolarSystemUI::OpenSolarSystemUI(){

}
OpenSolarSystemUI::OpenSolarSystemUI(System* system)
{

	this->sistema = system;
	
}
OpenSolarSystemUI::~OpenSolarSystemUI()
{
}

void OpenSolarSystemUI::open(const SolarSystem* sistem){

	if (this->sistema->getDatabaseConnection()){
		Star* s = this->sistema->getDatabase()->getStarOfSolarSystem(sistem->getID());
		list<Planet*> obj = this->sistema->getDatabase()->getPlanetsStarOfSolarSystem(sistem->getID());
		cout << "ID: " << sistem->getID() << endl;
		cout << "Name: " << sistem->getName() << endl;
		cout << "Star: " << s->getName() << endl;
		cout << "Number of bodies: " << obj.size() << endl;
		cout << "Press any key to continue..." << endl;
		char t;
		cin >> t;
	}
	else{
		cout << "ID: " << sistem->getID() << endl;
		cout << "Name: " << sistem->getName() << endl;
		cout << "Star: " << sistem->getStar()->getName() << endl;
		cout << "Number of bodies: " << sistem->getObjects().size() << endl;
		cout << "Press any key to continue..." << endl;
		char s;
		cin >> s;
	}

}

 

void OpenSolarSystemUI::mostrarSistemas() {
	int i = 1;
	list < SolarSystem * > sistemas;
	if (this->sistema->getDatabaseConnection()){
		 sistemas = this->sistema->getDatabase()->getSolarSystems();
	}
	else{
		 sistemas = this->sistema->getSolarSystems();
	}
	vector < SolarSystem * > vetor_sistemas;
	if (sistemas.size() != 0){
		cout << "Solar systems" << endl;
		for (list<SolarSystem *>::iterator itr = sistemas.begin(); itr != sistemas.end(); ++itr) {
			cout << i << "-" << (*itr)->getName() << endl;

			SolarSystem * ss = *itr;
			vetor_sistemas.push_back(ss);

			i++;
		}
		cout << endl;

		int escolha;
		cout << "Choose the number of the solar system you want to open: ";
		cin >> escolha;

		open(vetor_sistemas[escolha - 1]);
	
	}
	else{

		cout << "There are no Solar Systems to edit..." << endl;
		cout << "Press any key to continue..." << endl;
		char s;
		cin >> s;


	}
	
}


void OpenSolarSystemUI::run() {
    mostrarSistemas();
	
};
    
    
    
    


#endif	/* OPENSOLARSYSTEM_H */

