/*
*
* @author Vitor Moreira <1130564@isep.ipp.pt>
* @author Paulo Andrade <1130313@isep.ipp.pt>
* @author Emanuel Marques <1130553@isep.ipp.pt>
* @author Ivo Joao <1130404@isep.ipp.pt>
* @author Aria Jozi <1130777@isep.ipp.pt>
* @version 1.0
*
*/

#ifndef DELETESIMULATIONUI_H
#define	DELETESIMULATIONUI_H

using namespace std;

#include "DeleteSimulationController.h"

class DeleteSimulationUI {
public:
    DeleteSimulationUI(System * sistema);
    ~DeleteSimulationUI();

    void run();
    bool pedeConfirmacao() const;

private:
    System * sistema;
    DeleteSimulationController controller;

};

DeleteSimulationUI::DeleteSimulationUI(System * sistema) : controller(sistema) {
    this->sistema = sistema;
}

DeleteSimulationUI::~DeleteSimulationUI() {
}

bool DeleteSimulationUI::pedeConfirmacao() const {

    string resposta;
    do {
        cout << "\nDo you confirm the deletion of the simulation? y/n" << endl;
        cin >> resposta;

    } while (resposta != "y" && resposta != "n");

    if (resposta == "y") {
        return true;
    } else {
        return false;
    }

}

void DeleteSimulationUI::run() {

	list<Simulation*> simulations;
	if (this->sistema->getDatabaseConnection()){
		simulations = this->sistema->getDatabase()->getSimulations();
	}
	else{
		simulations = this->controller.getSimulationsList();
	}
    int i = 1;
    int resposta;

    cout << endl;
    for (list<Simulation*>::iterator itr = simulations.begin(); itr != simulations.end(); ++itr) {
		cout << (*itr)->getID() << ": " << (*itr)->getName() << endl;
    }

    Simulation * simulation = NULL;
    while (simulation == NULL) {
        cout << "Choose a simulation to be deleted: (write its name)" << endl;
        cin >> resposta;

        simulation = controller.getSimulationByID(resposta);
    }


    if (this->pedeConfirmacao()) {
        controller.removeSimulationByID(resposta);
		controller.deleteFromDB(simulation);
        cout << "\nSucess" << endl;
		cout << "Press any key to continue..." << endl;
		char s;
		cin >> s;
    } else {

        cout << "\nDeletion of simulation canceled!" << endl;
		cout << "Press any key to continue..." << endl;
		char s;
		cin >> s;
    }
}

#endif	/* DELETESIMULATIONUI_H */

