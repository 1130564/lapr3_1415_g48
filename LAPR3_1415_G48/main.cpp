/*
*
* @author Vitor Moreira <1130564@isep.ipp.pt>
* @author Paulo Andrade <1130313@isep.ipp.pt>
* @author Emanuel Marques <1130553@isep.ipp.pt>
* @author Ivo Joao <1130404@isep.ipp.pt>
* @author Aria Jozi <1130777@isep.ipp.pt>
* @version 1.0
*
*/
#include <cstdlib>
#include <iostream>
#include <vector>

using namespace std;

#include "IDManager.h"
#include "Algorithm.h"
#include "System.h"
#include "SolarSystem.h"
#include "Star.h"
#include "Body.h"
#include "NaturalSatellite.h"
#include "OtherBody.h"
#include "Planet.h"
#include "Moon.h"
#include "InputInterface.h"
#include "Simulation.h"
#include "InputInterface.h"
#include "CsvReader.h"
#include "SolarSystemUI.h"
#include "SimulationUI.h"
#include "CreateNewSimulationController.h"
#include "CreateNewSimulationUI.h"
#include "CopySolarSystemController.h"
#include "CopySolarSystemUI.h"
#include "OpenSolarSystemController.h"
#include "OpenSolarSystemUI.h"
#include "CreateNewSolarSystemController.h"
#include "CreateNewSolarSystemUI.h"
#include "HTMLWriter.h"
#include "ExportInterface.h"
#include "CsvWriter.h"
#include "BDados.h"
#include "GerarGif.h"
#include "RunSimulationUI.h"
#include "RunSimulationController.h"
#include "DeleteSimulationUI.h"
#include "DeleteSimulationController.h"
#include "ExportUI.h"
#include "ExportResultsToCSVUI.h"
#include "ExportResultsToCSVController.h"
#include "ExportResultsToHTMLUI.h"
#include "ExportResultsToHTMLController.h"




void clearScreen(){
	//clears console screen
	system("cls");
}


int menu() {
	int op;
	cout << "Select an option\n\n"
		"1 - Solar System\n"
		"2 - Simulation\n"
		"3 - Export\n"
		"4 - Exit\n" << endl;
	cin >> op;

	return op;

}

void run(System* sys){

	int op;
	do {
		clearScreen();
		op = menu();
		switch (op) {
		case 1:
		{
			clearScreen();
			SolarSystemUI a(sys);
			a.run();
			break;
		}
		case 2:
		{
			clearScreen();
			SimulationUI a(sys);
			a.run();
			break;
		}
		case 3:
		{
			clearScreen();
            ExportUI eUI(sys);
            eUI.run();
			break;
		}
		case 4:
		{
			if (sys->getDatabaseConnection()){
				sys->getDatabase()->pushIDS(lastSimulationID, lastStarID, lastBodyID, lastSolarSystemID);
			}
			//exit case
			break;
		}
		default:
		{
			cout << "Invalid Option!" << endl;
			cout << "Press any key to continue..." << endl;
			char s;
			cin>> s;
		op = NULL;
			break;
		}
		}
		
	} while (op != 4);

}


int main(int argc, char** argv) {
	System * sys = new System;
	try {
		cout << "Connecting to the Database..." << endl;
		BDados * db = new BDados("lapr3_048", "qwerty", "gandalf.dei.isep.ipp.pt:1521/pdborcl");
		
		sys->setDatabaseConnection(true);
		sys->setDatabase(db);
		db->getIDS();
		cout << "Success!\nPress any key to continue..." << endl;
		char s = getchar();
		run(sys);
		return 0;
		
	}
	catch (SQLException erro) {
		cerr << "Erro: " << erro.getMessage() << endl;
		char ans;
		do{
			cout<< "Do you wish to proceed without a connection? (y/n)"<<endl;
			cin >> ans;
		} while (ans != 'y' && ans != 'n');
		if (ans == 'y'){
			sys->setDatabaseConnection(false);
			run(sys);
		}
		else{
			cout << "Exiting..." << endl;
			return 0;
		}
	}
        

	    
}

