/*
*
* @author Vitor Moreira <1130564@isep.ipp.pt>
* @author Paulo Andrade <1130313@isep.ipp.pt>
* @author Emanuel Marques <1130553@isep.ipp.pt>
* @author Ivo Joao <1130404@isep.ipp.pt>
* @author Aria Jozi <1130777@isep.ipp.pt>
* @version 1.0
*
*/

#ifndef RUNSIMULATIONCONTROLLER_H
#define	RUNSIMULATIONCONTROLLER_H

#include "Simulation.h"
#include "System.h"

class RunSimulationController {
public:
    RunSimulationController(System * sistema);
    ~RunSimulationController();
    
    void setSimulation(Simulation* simulation);
    Simulation* getSimulation() const;

    list<Simulation*> getSimulationsList() const;
    void runSimulation();
	void pushToDB();

private:
    System * sistema;
    Simulation* simulation;

};

RunSimulationController::RunSimulationController(System * sistema) {
    this->sistema = sistema;
}

RunSimulationController::~RunSimulationController() {
}

void RunSimulationController::setSimulation(Simulation* simulation) {
    this->simulation = simulation;
}

Simulation* RunSimulationController::getSimulation() const {
    return this->simulation;
}

list<Simulation*> RunSimulationController::getSimulationsList() const{
    return this->sistema->getSimulations();
}

void RunSimulationController::runSimulation() {
    this->simulation->calcVelAndPos();
}

void RunSimulationController::pushToDB(){
	if (this->sistema->getDatabaseConnection()){
		cout << "Writing in Database..." << endl;
		vector<vector<vetor> > matriz = this->simulation->getVelocitysPositions();
		list<Body *> objects = this->simulation->getSolarSystem()->getObjects();


		// escrever velocidades e posiçoes a partir da matriz


		for (int i = 0; i < matriz[0].size(); i += 2) {
			list<Body* >::iterator corps = objects.begin();
			for (vector<vector<vetor> >::iterator it = matriz.begin(); it != matriz.end(); ++it) {
				vector<vetor> aux = (*it);
				Body* c = (*corps);
				float x = aux[i].x;
				float y = aux[i].y;
				float z = aux[i].z;
				this->sistema->getDatabase()->pushVelocity(c->getID(), (i / 2) + 1, x,  y,z, this->simulation->getID());
					++corps;
				
			}
			//--

			//posiçoes

			corps = objects.begin();
		
			for (vector<vector<vetor> >::iterator it = matriz.begin(); it != matriz.end(); ++it) {
				vector<vetor> aux = (*it);
				Body* c = (*corps);
				this->sistema->getDatabase()->pushPosition(c->getID(), ((i+1)/2)+1, aux[i].x, aux[i].y, aux[i].z, this->simulation->getID());
				++corps;
		

			}

		}
	}
}

#endif	/* RUNSIMULATIONCONTROLLER_H */

