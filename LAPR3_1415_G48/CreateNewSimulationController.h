/*
*
* @author Vitor Moreira <1130564@isep.ipp.pt>
* @author Paulo Andrade <1130313@isep.ipp.pt>
* @author Emanuel Marques <1130553@isep.ipp.pt>
* @author Ivo Joao <1130404@isep.ipp.pt>
* @author Aria Jozi <1130777@isep.ipp.pt>
* @version 1.0
*
*/

#ifndef CREATENEWSIMULATIONCONTROLLER_H
#define CREATENEWSIMULATIONCONTROLLER_H

#include "SolarSystem.h"
#include "Simulation.h"
#include "Star.h"
#include "System.h"

class CreateNewSimulationController {
public:

    CreateNewSimulationController(System * sistema);
    ~CreateNewSimulationController();

    string getName() const;
    double getStart() const;
    double getEnd() const;
    double getStep() const;
    Simulation* getSimulation() const;
    SolarSystem* getSolarSystem() const;
    list < Body * > getBodies() const;
    list < Body * > getSolarSystemBodies() const;
    list<SolarSystem *> getSolarSystems() const;
    Star getStar() const;

    void setStart(const double start);
    void setEnd(const double end);
    void setStep(const double step);
    void setSolarSystem(SolarSystem* solarSystem);
    void setbodies(const list < Body * > n);
    void setName(const string nome);
    void setSimulation(Simulation* simulation);
    void setStar(string name, double mass);
	void pushToDB();

    void addBody(Body* body);
    Simulation* createSimulation();
    void addSimulation();

private:

    System * sistema;
    Simulation* simulation;

};

CreateNewSimulationController::CreateNewSimulationController(System * sistema) {
    this->sistema = sistema;
}

CreateNewSimulationController::~CreateNewSimulationController() {
}

void CreateNewSimulationController::setStart(double start) {
    this->simulation->setStart_time(start);
}

void CreateNewSimulationController::setEnd(double end) {
    this->simulation->setEnd_time(end);
}

void CreateNewSimulationController::setStep(double step) {
    this->simulation->setTime_step(step);
}

void CreateNewSimulationController::setbodies(const list < Body * > n) {
    this->simulation->setBodies_subset(n);
}

void CreateNewSimulationController::setName(const string nome) {
    this->simulation->setName(nome);
}

string CreateNewSimulationController::getName() const {
    return this->simulation->getName();

}

double CreateNewSimulationController::getStart() const {
    return this->simulation->getStart_time();

}

double CreateNewSimulationController::getEnd() const {
    return this->simulation->getEnd_time();
}

double CreateNewSimulationController::getStep() const {
    return this->simulation->getTime_step();

}

list < Body * > CreateNewSimulationController::getBodies() const {
    return this->simulation->getBodies_subset();

}

list < Body * > CreateNewSimulationController::getSolarSystemBodies() const {
    return this->simulation->getSolarSystem()->getObjects();
}

Simulation* CreateNewSimulationController::createSimulation() {
    Simulation* sPtr = new Simulation;
    return sPtr;
}

void CreateNewSimulationController::setSimulation(Simulation* simulation) {
    this->simulation = simulation;
}

Simulation* CreateNewSimulationController::getSimulation() const {
    return simulation;
}

SolarSystem * CreateNewSimulationController::getSolarSystem() const {
    return this->simulation->getSolarSystem();
}

list<SolarSystem *> CreateNewSimulationController::getSolarSystems() const {
    return this->sistema->getSolarSystems();
}

void CreateNewSimulationController::addBody(Body* body) {
    this->simulation->addBody(body);
}

void CreateNewSimulationController::setSolarSystem(SolarSystem* solarSystem) {
    this->simulation->setSolarSystem(solarSystem);
}

void CreateNewSimulationController::addSimulation() {
    this->sistema->addSimulation(this->simulation);
	
}
void CreateNewSimulationController::pushToDB(){
	if (this->sistema->getDatabaseConnection()){
		this->sistema->getDatabase()->pushSimulation(this->simulation);
	}
}

#endif