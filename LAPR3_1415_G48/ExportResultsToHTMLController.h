/*
*
* @author Vitor Moreira <1130564@isep.ipp.pt>
* @author Paulo Andrade <1130313@isep.ipp.pt>
* @author Emanuel Marques <1130553@isep.ipp.pt>
* @author Ivo Joao <1130404@isep.ipp.pt>
* @author Aria Jozi <1130777@isep.ipp.pt>
* @version 1.0
*
*/

#ifndef EXPORTRESULTSTOHTMLCONTROLLER_H
#define	EXPORTRESULTSTOHTMLCONTROLLER_H

class ExportResultsToHTMLController {
public:
    ExportResultsToHTMLController(System * sistema);
    ~ExportResultsToHTMLController();
    
    list<Simulation*> getSimulationsList() const;
    void setSimulation(Simulation * simulation);
    Simulation* getSimulation() const;
    void exportResults();
    void setFileName(string fileName);
    string getFileName() const;
    void exportGIF();
    
private:
    System * sistema;
    Simulation * simulation;
    string fileName;

};

ExportResultsToHTMLController::ExportResultsToHTMLController(System * sistema) {
    this->sistema = sistema;
}

ExportResultsToHTMLController::~ExportResultsToHTMLController() {
}

list<Simulation*> ExportResultsToHTMLController::getSimulationsList() const {
    return this->sistema->getSimulations();
}

void ExportResultsToHTMLController::setSimulation(Simulation* simulation) {
    this->simulation = simulation;
}

Simulation* ExportResultsToHTMLController::getSimulation() const {
    return this->simulation;
}

void ExportResultsToHTMLController::exportResults() {
    ExportInterface * exp = new HTMLWriter();
    exp->writeVelPosMatrix(this->fileName, this->simulation);
}

void ExportResultsToHTMLController::setFileName(string fileName) {
    this->fileName = fileName;
}

string ExportResultsToHTMLController::getFileName() const {
    return this->fileName;
}
void ExportResultsToHTMLController::exportGIF()
{
    GerarGif g(1024,720,80);
    g.iniciar(this->fileName);
    g.desenhar(this->simulation);
    
    
}

#endif	/* EXPORTRESULTSTOHTMLCONTROLLER_H */

