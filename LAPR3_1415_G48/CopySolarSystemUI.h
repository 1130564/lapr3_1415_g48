﻿/*
*
* @author Vitor Moreira <1130564@isep.ipp.pt>
* @author Paulo Andrade <1130313@isep.ipp.pt>
* @author Emanuel Marques <1130553@isep.ipp.pt>
* @author Ivo Joao <1130404@isep.ipp.pt>
* @author Aria Jozi <1130777@isep.ipp.pt>
* @version 1.0
*
*/

#ifndef COPYSOLARSYSTEMUI_H
#define	COPYSOLARSYSTEMUI_H

#include "CopySolarSystemController.h"
#include "System.h"

class CopySolarSystemUI {
private:

    System *sistema;
    CopySolarSystemController controller;

public:

    CopySolarSystemUI(System* sistema);
    ~CopySolarSystemUI();

    void mostraSistemas();
    void mostraNovoSistema();
    bool pedeConfirmacao();

    void run();
};

CopySolarSystemUI::CopySolarSystemUI(System * sistema) : controller(sistema) {
    this->sistema = sistema;
}

CopySolarSystemUI::~CopySolarSystemUI() {

}

void CopySolarSystemUI::mostraSistemas() {
    int i = 1;


	list < SolarSystem * > sistemas;
	if (this->sistema->getDatabaseConnection()){
		sistemas = this->sistema->getDatabase()->getSolarSystems();
	}
	else{
		sistemas = this->sistema->getSolarSystems();
	}
    vector < SolarSystem * > vetor_sistemas;
    
    cout << "Solar systems"<<endl;
    for ( list<SolarSystem *>::iterator itr = sistemas.begin(); itr != sistemas.end(); ++itr) {
        cout << i << "-" << (*itr)->getName() << endl;
        
        SolarSystem * ss = *itr;
        vetor_sistemas.push_back(ss);
        
        i++;
    }
    cout << endl;
    
    int escolha;
    cout << "Choose the number of the solar system you want to copy from:";
    cin >> escolha;

    controller.createNewSolarSystem(vetor_sistemas[escolha - 1]);
}

bool CopySolarSystemUI::pedeConfirmacao() {
    string resposta;
    do {
        cout << "\nDo you confirm the creation of the solar system? y/n" << endl;
        cin >> resposta;
    } while (resposta != "y" && resposta != "n");

    if (resposta == "y") {
        return true;
    } else {
        return false;
    }

}

void CopySolarSystemUI::mostraNovoSistema() {
    controller.mostraNovoSistemaSolar();
}

void CopySolarSystemUI::run() {

    mostraSistemas();
    mostraNovoSistema();
    if (pedeConfirmacao()) {
        controller.confirma();
        cout << "Success" << endl;
		cout << "Press any key to continue..." << endl;
		char t;
		cin >> t;
    } else {
        cout << "Operation canceled" << endl;
		cout << "Press any key to continue..." << endl;
		char t;
		cin >> t;
    }
}



#endif	/* COPYSOLARSYSTEMUI_H */

