/*
*
* @author Vitor Moreira <1130564@isep.ipp.pt>
* @author Paulo Andrade <1130313@isep.ipp.pt>
* @author Emanuel Marques <1130553@isep.ipp.pt>
* @author Ivo Joao <1130404@isep.ipp.pt>
* @author Aria Jozi <1130777@isep.ipp.pt>
* @version 1.0
*
*/

#ifndef GERADORGIF_H
#define	GERADORGIF_H
#define _CRT_SECURE_NO_DEPRECATE

#include <vector>
#include "gif.h"
#include "Simulation.h"

typedef unsigned int uint;
typedef unsigned char uchar;

typedef struct {
    uchar r;
    uchar g;
    uchar b;
    uchar alpha;
} pixel;

class GerarGif {
private:
    GifWriter * c;
    //booleano que permite dizer que está ou não uma gif a ser criada.
    bool inicio = false;
    uint largura, altura, delay;
    void criarImagem(vector<vector<pixel *>>&imagem, uint largura, uint altura);
    void resetImagem(vector<vector<pixel *>>&imagem);
    void desenharImagem(vector<vector<pixel *>>&imagem);
    void destruirImagem(vector<vector<pixel * > >& imagem);
    vector<vector<pixel*>> importarImagem(string nome_ficheiro);
    void juntarImagens(vector<vector<pixel * > >& imagem, vector<vector<pixel * > >& outraImagem, int x, int y);

public:
    GerarGif(uint largura, uint altura, uint delay);
    ~GerarGif();
    //iniciar o gif
    void iniciar(string nome_gif);
    //bitDepth e dither opcional
    void iniciar(string nome_gif, uint bitDepth, uint dither);
    //funcao que desenha a simulacao
    void desenhar(Simulation * simulacao);
    //fim do gif libertação de recursos.
    void teste();
    void finalizar();
};

GerarGif::GerarGif(uint largura, uint altura, uint delay) {
    this->c = new GifWriter;
    this->largura = largura;
    this->altura = altura;
    this->delay = delay;
}

GerarGif::~GerarGif() {
    GifEnd(this->c);
}

void GerarGif::iniciar(string nome_gif) {
	nome_gif += ".gif";
    if (!inicio) {
        GifBegin(this->c, nome_gif.c_str(), this->largura, this->altura, this->delay);
        inicio = true;
    } else cout << "Uma gif já está a ser criada!" << endl;

}

void GerarGif::iniciar(string nome_gif, uint bitDepth, uint dither) {
    if (!inicio) {
        GifBegin(this->c, nome_gif.c_str(), this->largura, this->altura, this->delay, bitDepth, dither);
        inicio = true;
    } else cout << "Uma gif já está a ser criada!" << endl;
}

void GerarGif::criarImagem(vector<vector<pixel * > >& imagem, uint altura, uint largura) {
    for (int i = 0; i < altura; i++) {
        vector< pixel *> novo;
        for (int j = 0; j < largura; j++) {
            pixel * p = new pixel;
            p->r = 0;
            p->g = 0;
            p->b = 'H';
            p->alpha = 255;
            novo.push_back(p);
        }
        imagem.push_back(novo);
    }
}

void GerarGif::resetImagem(vector<vector<pixel*> >& imagem) {
    for (int i = 0; i < altura; i++) {
        for (int j = 0; j < largura; j++) {
            imagem[i][j]->r = 0;
            imagem[i][j]->g = 0;
            imagem[i][j]->b = 'H';
            imagem[i][j]->alpha = 255;
        }
    }
}

void GerarGif::destruirImagem(vector<vector<pixel * > >& imagem) {
    for (int i = 0; i < altura; i++) {
        for (int j = 0; j < largura; j++) {
            delete imagem[i][j];
        }
    }
}

void GerarGif::desenharImagem(vector<vector<pixel * > >& imagem) {
    uchar * b = new uchar[largura * altura * 4];
    int k = 0;
    for (int i = 0; i < altura; i++) {
        for (int j = 0; j < largura; j++) {
            b[k] = imagem[i][j]->r;
            b[k + 1] = imagem[i][j]->g;
            b[k + 2] = imagem[i][j]->b;
            b[k + 3] = imagem[i][j]->alpha;
            k += 4;
        }
    }
    GifWriteFrame(this->c, b, this->largura, this->altura, this->delay);
    //libertar memória.
    delete [] b;
}

/*
 *recebe duas imagens e junta-as a partir do centro (x,y).
 *  
 */
void GerarGif::juntarImagens(vector<vector<pixel * > >& imagem, vector<vector<pixel * > >& outraImagem, int x, int y) {

    int centro_x = this->largura / 2 + x - outraImagem[0].size() / 2;
    int centro_y = this->altura / 2 - y - outraImagem.size() / 2;

    for (int i = 0; i < outraImagem.size(); i++) {

        for (int j = 0; j < outraImagem[i].size(); j++) {
            if (centro_y + j > 0 && centro_y + j<this->altura && centro_x + i > 0 && centro_x + i<this->largura) {
                imagem[centro_y + j][centro_x + i]->r = outraImagem[i][j]->r;
                imagem[centro_y + j][centro_x + i]->g = outraImagem[i][j]->g;
                imagem[centro_y + j][centro_x + i]->b = outraImagem[i][j]->b;
                imagem[centro_y + j][centro_x + i]->alpha = outraImagem[i][j]->alpha;
            }
        }

    }


}

void GerarGif::finalizar() {
    GifEnd(this->c);
}

/*
 * Source : http://stackoverflow.com/questions/9296059/read-pixel-value-in-bmp-file
 */

vector<vector<pixel*>> GerarGif::importarImagem(string nome_ficheiro) {
 
    FILE* f = fopen(nome_ficheiro.c_str(), "rb");
    unsigned char info[54];
    fread(info, sizeof (unsigned char), 54, f); // read the 54-byte header

    // extract image height and width from header
    int width = *(int*) &info[18];
    int height = *(int*) &info[22];
    int size = 3 * width * height;
    unsigned char* data = new unsigned char[size]; // allocate 3 bytes per pixel
    fread(data, sizeof (unsigned char), size, f); // read the rest of the data at once
    fclose(f);
    vector<vector < pixel*>> imagem;
    vector<pixel *> linha;
    int k = 0;
    for (int j = 0; j < size; j += 3) {
        pixel * p = new pixel;
        p->b = data[j];
        p->g = data[j + 1];
        p->r = data[j + 2];
        p->alpha = 255;
        if (k == width) {
            imagem.push_back(linha);
            linha.clear();
            linha.push_back(p);
            k = 1;
        } else {
            linha.push_back(p);
            k++;
        }
    }
    if (imagem.size() > 0)
        imagem.push_back(linha);
    return imagem;
}

void GerarGif::desenhar(Simulation * simulacao) {

    cout << "Generating gif..." << endl;
    vector < vector< pixel *> > imagem;
    this->criarImagem(imagem, this->altura, this->largura);
    this->desenharImagem(imagem);
    vector < vector<pixel *> > planeta = importarImagem("planeta.bmp");
    vector < vector<pixel *> > sol = importarImagem("sol.bmp");
    vector < vector < vetor> > posicoes = simulacao->getVelocitysPositions();
    for (int j = 0; j < posicoes[0].size(); j += 2) {
        resetImagem(imagem);
        this->juntarImagens(imagem, sol, 0, 0);
        for (int i = 0; i < posicoes.size(); i++) {
            vetor posicao = posicoes[i][j + 1];
            // if only verlet, divide by 1E10
            // if verlet and kepler, multiply by 1E30
            double posicao_x = posicao.x * 1E10;
            double posicao_y = posicao.y * 1E10;
            int x = (int) posicao_x;
            int y = (int) posicao_y;
            this->juntarImagens(imagem, planeta, x, y);
        }
        this->desenharImagem(imagem);
    }


}
#endif	/* GERADORGIF_H */