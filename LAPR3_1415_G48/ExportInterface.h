/*
*
* @author Vitor Moreira <1130564@isep.ipp.pt>
* @author Paulo Andrade <1130313@isep.ipp.pt>
* @author Emanuel Marques <1130553@isep.ipp.pt>
* @author Ivo Joao <1130404@isep.ipp.pt>
* @author Aria Jozi <1130777@isep.ipp.pt>
* @version 1.0
*
*/

#ifndef EXPORTINTERFACE_H
#define	EXPORTINTERFACE_H

using namespace std;

#include <list>

class ExportInterface {
public:
    ExportInterface();
    virtual ~ExportInterface();
    
    virtual void writeVelPosMatrix(const string file, Simulation * simulation) = 0;
private:


};

ExportInterface::ExportInterface() {

}

ExportInterface::~ExportInterface() {

}


#endif	/* EXPORTINTERFACE_H */

