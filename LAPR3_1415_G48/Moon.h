/*
*
* @author Vitor Moreira <1130564@isep.ipp.pt>
* @author Paulo Andrade <1130313@isep.ipp.pt>
* @author Emanuel Marques <1130553@isep.ipp.pt>
* @author Ivo Joao <1130404@isep.ipp.pt>
* @author Aria Jozi <1130777@isep.ipp.pt>
* @version 1.0
*
*/

#ifndef MOON_H
#define	MOON_H

#include "NaturalSatellite.h"


using namespace std;

class Moon : public NaturalSatellite {
private:

    // moons orbiting around this moon
    list<Moon*> moons;

public:

    Moon();
    Moon(string name, double mass, vetor initialVelocity, vetor initialPosition, double period_of_revolution, double orbital_speed, double incl_of_axis_to_orbit, double equa_diameter, double density, double escape_velocity, double semimajor_axis, double orbit_eccentricity, double orbit_inclination, double perihelion, double aphelion, double meanAnomaly, double longitudeOfPerihelion, double longitudeOfAscendingNode);
    Moon(const Moon& toCopy);

    ~Moon();
    Moon * clone() const;

    void escreve(ostream& ostr) const;
};

Moon::Moon() : NaturalSatellite(), moons() {

}

Moon::Moon(string name, double mass, vetor initialVelocity, vetor initialPosition, double period_of_revolution, double orbital_speed, double incl_of_axis_to_orbit, double equa_diameter, double density, double escape_velocity, double semimajor_axis, double orbit_eccentricity, double orbit_inclination, double perihelion, double aphelion, double meanAnomaly, double longitudeOfPerihelion, double longitudeOfAscendingNode)
: NaturalSatellite(name, mass, initialVelocity, initialPosition, period_of_revolution, orbital_speed, incl_of_axis_to_orbit, equa_diameter, density, escape_velocity, semimajor_axis, orbit_eccentricity, orbit_inclination, perihelion, aphelion, meanAnomaly, longitudeOfPerihelion, longitudeOfAscendingNode), moons() {

}

Moon::Moon(const Moon& toCopy) : NaturalSatellite(toCopy), moons(toCopy.moons) {

}

Moon::~Moon() {

}

Moon * Moon::clone() const {
    return new Moon(*this);
}

void Moon::escreve(ostream& ostr) const {
    ostr << "Moon" << endl;
    NaturalSatellite::escreve(ostr);

}


#endif	/* MOON_H */

