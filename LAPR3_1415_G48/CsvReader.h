/*
*
* @author Vitor Moreira <1130564@isep.ipp.pt>
* @author Paulo Andrade <1130313@isep.ipp.pt>
* @author Emanuel Marques <1130553@isep.ipp.pt>
* @author Ivo Joao <1130404@isep.ipp.pt>
* @author Aria Jozi <1130777@isep.ipp.pt>
* @version 1.0
*
*/

#ifndef CSVREADER_H
#define	CSVREADER_H

#include <cstdlib>
#include <iostream>
#include <sstream>
#include <vector>
#include <string>
#include <fstream>

using namespace std;

#include "InputInterface.h"
#include "NaturalSatellite.h"
#include "Planet.h"
#include "System.h"

class CsvReader : public InputInterface {
private:
    System* sys;
    SolarSystem* ss;
    void split(const string &s, char delim, vector<string> &elems);
    void importOneListObject(list<Body *>& bodies, const string& file);

public:
    CsvReader(System * sys, SolarSystem* s);
    ~CsvReader();
    list<Body *> read(string file);
    void parser(list<Body *>& bodies, const vector <string>& infoPlaneta);
};

CsvReader::CsvReader(System* syst, SolarSystem* s) : InputInterface() {
    this->sys = syst;
    this->ss = s;
}

CsvReader::~CsvReader() {

}

void CsvReader::split(const string &s, char delim, vector<string> &elems) {
    stringstream ss(s);
    string item;
    while (getline(ss, item, delim)) {
        if (!item.empty())
            elems.push_back(item);
    }
}

void CsvReader::importOneListObject(list<Body *>& bodies, const string& file) {

    ifstream fx;
    fx.open(file.c_str());

    if (!fx) {
        cout << "O ficheiro " << file << " nao existe !" << endl;
        return;
    }

    string linha;

    vector<string> elementos; // vector onde é guardado o split de cada linha
    vector< vector<string> > planetas; // vector onde é guardada a informação para criar cada planeta
    vector<string> chaves; // chaves para a criação dos planetas

    bool encontrouPlanetas = false;

    while (!fx.eof()) {

        getline(fx, linha, '\r');
        split(linha, ';', elementos);

        //se ainda não encontrou a linha que diz quais são todos os planetas e a o split pelo ; tem pelo menos 1 elemento
        if (!encontrouPlanetas && elementos.size() > 0) {
            encontrouPlanetas = true;
            planetas = vector<vector<string> >(elementos.size() - 1); // primeira posição está vazia.

            for (unsigned int i = 0; i < planetas.size(); i++) {
                //enviar o nome para o vector<string> de cada planeta
                planetas[i].push_back(elementos[i + 1]);
            }

        } else {

            for (unsigned int j = 1; j < elementos.size(); j++) {
                planetas[j - 1].push_back(elementos[j]);
            }

        }
        elementos.clear();
    }
    // Parsing
    for (vector<string > info : planetas) {
        parser(bodies, info);
    }

}

void CsvReader::parser(list<Body *>& bodies, const vector <string>& infoPlaneta) {
    if(infoPlaneta.size()>18){
        
    string name = infoPlaneta[0];
    double mass = atof(infoPlaneta[5].c_str());
    double period_of_revolution = atof(infoPlaneta[1].c_str());
    double orbital_speed = atof(infoPlaneta[2].c_str());
    double incl_of_axis_to_orbit = atof(infoPlaneta[3].c_str());
    double equa_diameter = atof(infoPlaneta[4].c_str());
    double density = atof(infoPlaneta[6].c_str());
    double escape_velocity = atof(infoPlaneta[7].c_str());
    double semimajor_axis = atof(infoPlaneta[8].c_str());
    double orbit_eccentricity = atof(infoPlaneta[9].c_str());
    double orbit_inclination = atof(infoPlaneta[10].c_str());
    double perihelion = atof(infoPlaneta[11].c_str());
    double aphelion = atof(infoPlaneta[12].c_str());
    vetor initial_position = {atof(infoPlaneta[13].c_str()), atof(infoPlaneta[14].c_str()), atof(infoPlaneta[15].c_str())};
    vetor initial_velocity = {atof(infoPlaneta[16].c_str()), atof(infoPlaneta[17].c_str()), atof(infoPlaneta[18].c_str())};

    // converter AU para m da posiçao inicial
    initial_position.x = initial_position.x * 1.4960E11;
    initial_position.y = initial_position.y * 1.4960E11;
    initial_position.z = initial_position.z * 1.4960E11;



    Planet * b = new Planet(name, mass, initial_velocity, initial_position, period_of_revolution, orbital_speed, incl_of_axis_to_orbit, equa_diameter, density, escape_velocity, semimajor_axis, orbit_eccentricity, orbit_inclination, perihelion, aphelion, 0, 0, 0);
        if (this->sys->getDatabaseConnection()) {
            this->sys->getDatabase()->pushPlanet(b, this->ss->getID());
    		this->sys->getDatabase()->pushVelocity(b->getID(), 0, b->getInitialVelocity().x, b->getInitialVelocity().y, b->getInitialVelocity().z, -1);
    		this->sys->getDatabase()->pushPosition(b->getID(), 0, b->getInitialPosition().x, b->getInitialPosition().y, b->getInitialPosition().z, -1);
    
        }

    bodies.push_back(b);
    }
    else{
        cout << "Wrong reader choosed."<<endl;
    }
}

list <Body * > CsvReader::read(string file) {

    list<Body * > bodies;
    this->importOneListObject(bodies, file);
    return bodies;
}

#endif	/* CSVREADER_H */

