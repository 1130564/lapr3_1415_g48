/*
*
* @author Vitor Moreira <1130564@isep.ipp.pt>
* @author Paulo Andrade <1130313@isep.ipp.pt>
* @author Emanuel Marques <1130553@isep.ipp.pt>
* @author Ivo Joao <1130404@isep.ipp.pt>
* @author Aria Jozi <1130777@isep.ipp.pt>
* @version 1.0
*
*/

#ifndef CREATENEWSOLARSYSTEM_H
#define CREATENEWSOLARSYSTEM_H

#include "CreateNewSolarSystemController.h"
#include "CsvReader.h"
#include "CsvReaderTypeII.h"
#include "CsvReaderTypeIII.h"

class CreateNewSolarSystemUI {
public:

    CreateNewSolarSystemUI(System * sistema);
    ~CreateNewSolarSystemUI();
    bool pedeConfirmacao();
    void run();

private:
    CreateNewSolarSystemController controller;
    System * sistema;

};

CreateNewSolarSystemUI::CreateNewSolarSystemUI(System * sistema) : controller(sistema) {
    this->sistema = sistema;
}

CreateNewSolarSystemUI::~CreateNewSolarSystemUI() {
}

bool CreateNewSolarSystemUI::pedeConfirmacao() {
    string resposta;
    do {
        cout << "Do you confirm the creation of the solar system? y/n" << endl;
        cin >> resposta;
    } while (resposta != "y" && resposta != "n");

    if (resposta == "y") {
        return true;
    } else {
        return false;
    }

}

void CreateNewSolarSystemUI::run() {

    cout << "\nInsert the Solar System's name:" << endl;
    string ssname;
    cin >> ssname;
    controller.setName(ssname);

    cout << "\nInsert the Star's name:" << endl;
    string name;
    cin >> name;

    float mass;
    do {
        cout << "\nInsert the Star's mass:" << endl;
        cin >> mass;
    } while (mass <= 0);

    cout << "\nInsert the name of the CSV file that contains the bodies information:" << endl;
    string file;
    cin >> file;
    Star* st = controller.createStar(name, mass);
    controller.setStar(st);
    
	cout << "\nInsert the type of the CSV file" << endl;
    cout << "1 - CsvReaderType I" << endl;
    cout << "2 - CsvReaderType II" << endl;
    cout << "3 - CsvReaderType III" << endl;
    
    int escolha_csv;
    do {
        cin>>escolha_csv;
    } while (escolha_csv < 0 || escolha_csv > 3);
    
    InputInterface * ip;
    switch (escolha_csv) {
        case 1:
        {
            ip = new CsvReader(sistema, controller.getSolarSystem());
			controller.setObjects(ip->read(file));
            break;
        }
        case 2:
        {
			ip = new CsvReaderTypeII(sistema, controller.getSolarSystem());
			controller.setObjects(ip->read(file));

            break;
        }
        case 3:
        {
            ip = new CsvReaderTypeIII(sistema, controller.getSolarSystem());
			controller.setObjects(ip->read(file));
            break;
        }
    }
    
    if (pedeConfirmacao()) {
        controller.addSolarSystem();
        if (this->sistema->getDatabaseConnection() == true) {
            controller.pushToDB();
        }
        list<Body *> objects = controller.getSolarSystem()->getObjects();
        cout << "\nSolar system created with sucess!" << endl;
        cout << "Press any key to continue..." << endl;
        char s;
        cin >> s;
    }

}

#endif /* CREATENEWSOLARSYSTEM_H */
