/* 
 * File:   CsvReaderTypeIII.h
 * Author: Paulo
 *
 * Created on 16 de Janeiro de 2015, 4:16
 */

#ifndef CSVREADERTYPEIII_H
#define	CSVREADERTYPEIII_H

#include "InputInterface.h"

class CsvReaderTypeIII : public InputInterface {
private:
    System* sys;
    SolarSystem* ss;
    void split(const string &s, char delim, vector<string> &elems);
    void importOneListObject(list<Body *>& bodies, const string& file);
public:
    CsvReaderTypeIII(System * sys, SolarSystem* s);
    ~CsvReaderTypeIII();
    list<Body *> read(const string file);
    void parser(list<Body *>& bodies, const vector <string>& infoPlaneta);
};

CsvReaderTypeIII::CsvReaderTypeIII(System * sys, SolarSystem* s) : InputInterface() {
    this->sys = sys;
    this->ss = s;
}

CsvReaderTypeIII::~CsvReaderTypeIII() {

}

void CsvReaderTypeIII::split(const string &s, char delim, vector<string> &elems) {
    stringstream ss(s);
    string item;
    while (getline(ss, item, delim)) {
        if (!item.empty())
            elems.push_back(item);
    }
}

void CsvReaderTypeIII::importOneListObject(list<Body *>& bodies, const string& file) {

    ifstream fx;
    fx.open(file.c_str());

    if (!fx) {
        cout << "O ficheiro " << file << " nao existe !" << endl;
        return;
    }

    string linha;

    vector<string> elementos; // vector onde é guardado o split de cada linha
    vector< vector<string> > planetas; // vector onde é guardada a informação para criar cada planeta
    vector<string> chaves; // chaves para a criação dos planetas

    bool encontrouPlanetas = false;

    while (!fx.eof()) {
        getline(fx, linha, '\r');
        split(linha, ';', elementos);
        //se ainda não encontrou a linha que diz quais são todos os planetas e a o split pelo ; tem pelo menos 1 elemento
        if (!encontrouPlanetas && elementos.size() > 0) {
            encontrouPlanetas = true;
            planetas = vector<vector<string> >(elementos.size() - 1); // primeira posição está vazia.

            for (unsigned int i = 0; i < planetas.size(); i++) {
                //enviar o nome para o vector<string> de cada planeta
                planetas[i].push_back(elementos[i + 1]);
            }

        } else {

            for (unsigned int j = 1; j < elementos.size(); j++) {
                planetas[j - 1].push_back(elementos[j]);
            }

        }
        elementos.clear();
    }
    // Parsing
    for (vector<string > info : planetas) {
        parser(bodies, info);
    }

}

void CsvReaderTypeIII::parser(list<Body *>& bodies, const vector <string>& infoPlaneta) {
    if (infoPlaneta.size() > 22) {
    string name = infoPlaneta[0];
    double mass = atof(infoPlaneta[6].c_str());
    double period_of_revolution = atof(infoPlaneta[2].c_str());
    double orbital_speed = atof(infoPlaneta[3].c_str());
    double incl_of_axis_to_orbit = atof(infoPlaneta[4].c_str());
    double equa_diameter = atof(infoPlaneta[5].c_str());
    double density = atof(infoPlaneta[7].c_str());
    double escape_velocity = atof(infoPlaneta[8].c_str());
    double semimajor_axis = atof(infoPlaneta[9].c_str());
    double orbit_eccentricity = atof(infoPlaneta[10].c_str());
    double orbit_inclination = atof(infoPlaneta[11].c_str());
    /*
     *novos atributos!
     */
    double mean_anomaly_J2000 = atof(infoPlaneta[12].c_str());
    double longitude_of_perihelion = atof(infoPlaneta[13].c_str());
    double longitude_of_ascending_node = atof(infoPlaneta[14].c_str());
    //
    double perihelion = atof(infoPlaneta[15].c_str());
    double aphelion = atof(infoPlaneta[16].c_str());
    vetor initial_position = {atof(infoPlaneta[17].c_str()), atof(infoPlaneta[18].c_str()), atof(infoPlaneta[19].c_str())};
    vetor initial_velocity = {atof(infoPlaneta[20].c_str()), atof(infoPlaneta[21].c_str()), atof(infoPlaneta[22].c_str())};

    // converter AU para m da posiçao inicial
    initial_position.x = initial_position.x * 1.4960E11;
    initial_position.y = initial_position.y * 1.4960E11;
    initial_position.z = initial_position.z * 1.4960E11;

    string orbiting = infoPlaneta[1];
    string star = "Star";

    if (orbiting == star) {
        Planet * b = new Planet(name, mass, initial_velocity, initial_position, period_of_revolution, orbital_speed, incl_of_axis_to_orbit, equa_diameter, density, escape_velocity, semimajor_axis, orbit_eccentricity, orbit_inclination, perihelion, aphelion, mean_anomaly_J2000, longitude_of_perihelion, longitude_of_ascending_node);
        if (this->sys->getDatabaseConnection()) {
            this->sys->getDatabase()->pushPlanet(b, this->ss->getID());
        }
        bodies.push_back(b);
    } else {
        for (list<Body *>::iterator itr = bodies.begin(); itr != bodies.end(); ++itr) {
            if ((*itr)->getName() == orbiting) {
                Moon * m = new Moon(name, mass, initial_velocity, initial_position, period_of_revolution, orbital_speed, incl_of_axis_to_orbit, equa_diameter, density, escape_velocity, semimajor_axis, orbit_eccentricity, orbit_inclination, perihelion, aphelion, mean_anomaly_J2000, longitude_of_perihelion, longitude_of_ascending_node);
                (dynamic_cast<Planet *> (*itr))->addMoons(m);
            }
        }
    }
    } else {
        cout << "Wrong reader choosed." << endl;
    }
}

list <Body * > CsvReaderTypeIII::read(const string file) {
    list<Body * > bodies;
    this->importOneListObject(bodies, file);
    return bodies;
}


#endif	/* CSVREADERTYPEIII_H */

