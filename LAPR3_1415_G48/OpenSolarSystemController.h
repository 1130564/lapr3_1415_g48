/*
*
* @author Vitor Moreira <1130564@isep.ipp.pt>
* @author Paulo Andrade <1130313@isep.ipp.pt>
* @author Emanuel Marques <1130553@isep.ipp.pt>
* @author Ivo Joao <1130404@isep.ipp.pt>
* @author Aria Jozi <1130777@isep.ipp.pt>
* @version 1.0
*
*/

#ifndef OPENSOLARSYSTEMCONTROLLER_H
#define	OPENSOLARSYSTEMCONTROLLER_H
#include "SolarSystem.h"

class OpenSolarSystemController {

public:
    OpenSolarSystemController();
    OpenSolarSystemController(System* s);
     ~OpenSolarSystemController();
    System* getSystem();
    void setSystem(System* sis);

private:
    System* sys;

};
OpenSolarSystemController::OpenSolarSystemController()
{


}
OpenSolarSystemController::OpenSolarSystemController(System* s)
{
	sys = s;
	
}
OpenSolarSystemController::~OpenSolarSystemController()
{

}
System* OpenSolarSystemController::getSystem(){
	return this->sys;
}
void OpenSolarSystemController::setSystem(System* sis){
	this->sys = sis;
}

#endif	/* OPENSOLARSYSTEMCONTROLLER_H */

