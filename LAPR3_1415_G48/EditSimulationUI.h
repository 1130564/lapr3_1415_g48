/*
*
* @author Vitor Moreira <1130564@isep.ipp.pt>
* @author Paulo Andrade <1130313@isep.ipp.pt>
* @author Emanuel Marques <1130553@isep.ipp.pt>
* @author Ivo Joao <1130404@isep.ipp.pt>
* @author Aria Jozi <1130777@isep.ipp.pt>
* @version 1.0
*
*/

#ifndef EDITSIMULATIONUI_H
#define	EDITSIMULATIONUI_H

#include "System.h"
#include "EditSimulationController.h"


using namespace std;

class EditSimulationUI {
private:

    System * sistema;
    EditSimulationController controller;

public:

    EditSimulationUI(System * sistema);
    ~EditSimulationUI();

    void mostraSimulacoes();
    bool changeNameQuestion();
    bool changeTimeStepQuestion();
    void pedeInformacao();
    bool pedeConfirmacao();
    void run();
    void menuAlteracoes();
    void alterarDados(Simulation * s);
};

EditSimulationUI::EditSimulationUI(System* sistema) : controller(sistema) {
    this->sistema = sistema;
}

EditSimulationUI::~EditSimulationUI() {

}

void EditSimulationUI::mostraSimulacoes() {
    int i = 1;
	list < Simulation * > simulacoes;
	if (this->sistema->getDatabaseConnection()){
		simulacoes = this->sistema->getDatabase()->getSimulations();
	}
	else{
		simulacoes = this->sistema->getSimulations();
	}
    vector < Simulation * > vetor_simulacoes;
    cout << "Simulations" << endl;
    for (list< Simulation * >::iterator itr = simulacoes.begin(); itr != simulacoes.end(); ++itr) {
		cout << (*itr)->getID() << "-" << (*itr)->getName() << endl;
        Simulation * s = *itr;
        vetor_simulacoes.push_back(s);
        i++;
    }
    cout << endl;

    int escolha;
    cout << "Choose the number of the simulation you want to edit: ";
    cin >> escolha;
    controller.setSimulation(vetor_simulacoes[escolha - 1]);
    controller.resetVelocitysPositions();
}

bool EditSimulationUI::changeNameQuestion() {
    string resposta;
    do {
        cout << "\nDo you want to change the name of the simulation ? y/n" << endl;
        cin >> resposta;
    } while (resposta != "y" && resposta != "n");

    if (resposta == "y") {
        return true;
    } else {
        return false;
    }

}

bool EditSimulationUI::changeTimeStepQuestion() {
    string resposta;
    do {
        cout << "\nDo you want to change the timestep? y/n" << endl;
        cin >> resposta;
    } while (resposta != "y" && resposta != "n");

    if (resposta == "y") {
        return true;
    } else {
        return false;
    }
}

void EditSimulationUI::run() {
	list < Simulation * > simulacoes;
	if (this->sistema->getDatabaseConnection()){
		simulacoes = this->sistema->getDatabase()->getSimulations();
	}
	else{
		simulacoes = this->sistema->getSimulations();
	}
    if (simulacoes.empty()) {
        cout << "No simulations available." << endl;
		cout << "Press any key to continue..." << endl;
		char s;
		cin >> s;
    } else {
        mostraSimulacoes();
        menuAlteracoes();
    }
}
void EditSimulationUI::menuAlteracoes()
{
    if (changeNameQuestion()) {
            cout << "\nInsert the simulation's name:" << endl;
            string name;
            cin >> name;
            controller.setName(name);
        }
        if (changeTimeStepQuestion()) {
            cout << "\nInsert the simulation's start time:" << endl;
            double start_time;
            cin >> start_time;
            controller.setStart(start_time);

            cout << "\nInsert the simulation's end time:" << endl;
            double end_time;
            cin >> end_time;
            controller.setEnd(end_time);

            cout << "\nInsert the simulation's timestep:" << endl;
            double step_time;
            cin >> step_time;
            controller.setStep(step_time);
        }
}
void EditSimulationUI::alterarDados(Simulation * s) {
    this->controller.setSimulation(s);
    menuAlteracoes();

	cout << "Success!\nPress any key to continue..." << endl;
	char t;
	cin >> t;
}

#endif	/* EDITSIMULATIONUI_H */

