﻿/*
*
* @author Vitor Moreira <1130564@isep.ipp.pt>
* @author Paulo Andrade <1130313@isep.ipp.pt>
* @author Emanuel Marques <1130553@isep.ipp.pt>
* @author Ivo Joao <1130404@isep.ipp.pt>
* @author Aria Jozi <1130777@isep.ipp.pt>
* @version 1.0
*
*/

#ifndef IDMANAGER_H
#define IDMANAGER_H

int lastBodyID;
int lastStarID;
int lastSolarSystemID;
int lastSimulationID;

void resetIDS(){
	lastBodyID = 0;
	lastStarID = 0;
	lastSolarSystemID = 0;
	lastSimulationID = 0;
}

int newBody(){
	lastBodyID++;
	return lastBodyID;
}
int newStar(){
	lastStarID++;
	return lastStarID;
}
int newSolarSystem(){
	lastSolarSystemID++;
	return lastSolarSystemID;
}
int newSimulation(){
	lastSimulationID++;
	return lastSimulationID;
}


#endif