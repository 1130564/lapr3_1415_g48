/*
*
* @author Vitor Moreira <1130564@isep.ipp.pt>
* @author Paulo Andrade <1130313@isep.ipp.pt>
* @author Emanuel Marques <1130553@isep.ipp.pt>
* @author Ivo Joao <1130404@isep.ipp.pt>
* @author Aria Jozi <1130777@isep.ipp.pt>
* @version 1.0
*
*/

#ifndef SOLARSYSTEM_H
#define SOLARSYSTEM_H


#include <list>
#include <cstdlib>
#include <ostream>
using namespace std;

#include "Body.h"
#include "Star.h"

class SolarSystem {
private:

    list<Body*> objects;
    Star* star;
    string name;
    int id;

public:

    SolarSystem();
	SolarSystem(int id, string name);
    SolarSystem(const SolarSystem& toCopy);
    ~SolarSystem();
    SolarSystem* clone() const;

    int getID() const;
    string getName() const;
    Star* getStar() const;
    list<Body*> getObjects() const;



    void setName(string name);
    void setStar(Star* st);
    void setObjects(list<Body*> obj);

    bool addObject(Body* obj);
    bool removeObjectByName(string nome);

    virtual void escreve(ostream& ostr) const;

};

SolarSystem::SolarSystem() {
	this->id = newSolarSystem();
}

SolarSystem::SolarSystem(int id1, string nome){
	this->id = id1;
	this->name = nome;
}

SolarSystem::SolarSystem(const SolarSystem& toCopy) {
	this->id = newSolarSystem();
    this->name = toCopy.name;
    this->star = toCopy.getStar()->clone();
    list<Body * > tmp = toCopy.getObjects();
    list<Body * > lista_copia;
    for (list<Body * >::iterator itr = tmp.begin(); itr != tmp.end(); ++itr) {
        lista_copia.push_back((*itr)->clone());
    }
    this->setObjects(lista_copia);
}

SolarSystem::~SolarSystem() {

}

SolarSystem* SolarSystem::clone() const {
    return new SolarSystem(*this);
}

list<Body*> SolarSystem::getObjects() const {
    return this->objects;
}

string SolarSystem::getName() const {
    return this->name;
}

int SolarSystem::getID() const{
	return this->id;
}

void SolarSystem::setObjects(list<Body*> obj) {
    this->objects = obj;

}

void SolarSystem::setName(string nome) {
    this->name = nome;
}

bool SolarSystem::addObject(Body* obj) {
    this->objects.push_back(obj);
    return true;
}

bool SolarSystem::removeObjectByName(string name) {

    list<Body*>::iterator itr;
    for (itr = this->objects.begin(); itr != this->objects.end(); ++itr) {
        if ((*itr)->getName() == name) {
            this->objects.remove((*itr));
        }
    }
    return true;
}

Star* SolarSystem::getStar() const {
    return this->star;
}

void SolarSystem::setStar(Star* st) {
    this->star = st;
}



void SolarSystem::escreve(ostream& ostr) const {
    ostr << "Name: " << this->name << endl;
}

ostream& operator<<(ostream& ostr, const SolarSystem &b) {
    b.escreve(ostr);
    return ostr;
}

#endif