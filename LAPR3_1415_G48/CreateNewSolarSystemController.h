﻿/*
*
* @author Vitor Moreira <1130564@isep.ipp.pt>
* @author Paulo Andrade <1130313@isep.ipp.pt>
* @author Emanuel Marques <1130553@isep.ipp.pt>
* @author Ivo Joao <1130404@isep.ipp.pt>
* @author Aria Jozi <1130777@isep.ipp.pt>
* @version 1.0
*
*/

#ifndef CREATENEWSOLARSYSTEMCONTROLLER_H
#define CREATENEWSOLARSYSTEMCONTROLLER_H

#include "SolarSystem.h"
#include "System.h"

class CreateNewSolarSystemController
{
public:
	CreateNewSolarSystemController(System * s);
	~CreateNewSolarSystemController();
	
        SolarSystem* getSolarSystem();
        
        void setName(string name);
	void setSolarSystem(SolarSystem* solar);
	void setStar(Star* s);
        void setObjects(list<Body*> objects);
	
   
		Star* createStar(string name, float mass);
        void addSolarSystem();
		void pushToDB();

private:
	System * sys;
	SolarSystem * ss;
};
CreateNewSolarSystemController::CreateNewSolarSystemController(System * s)
{
	sys = s;
        ss = new SolarSystem;
}

CreateNewSolarSystemController::~CreateNewSolarSystemController()
{
    
}


void CreateNewSolarSystemController::setSolarSystem(SolarSystem* solar){
	this->ss = solar;
}

void CreateNewSolarSystemController::setStar(Star* s){
	this->ss->setStar(s);
}

void CreateNewSolarSystemController::setObjects(list<Body*> objects){
	this->ss->setObjects(objects);
}

Star* CreateNewSolarSystemController::createStar(string name, float mass){
        Star* s = new Star(name, mass);
	return s;
}

void CreateNewSolarSystemController::addSolarSystem()
{
    this->sys->addSolarSystem(this->ss);
}

void CreateNewSolarSystemController::setName(string name) {
	this->ss->setName(name);
}

SolarSystem* CreateNewSolarSystemController::getSolarSystem(){
	return this->ss;
}

void CreateNewSolarSystemController::pushToDB(){
	this->sys->getDatabase()->pushSolarSystem(this->ss);
	this->sys->getDatabase()->pushStar(this->ss->getStar(), this->ss->getID());

}

#endif /* CREATENEWSOLARSYSTEMCONTROLLER_H */
