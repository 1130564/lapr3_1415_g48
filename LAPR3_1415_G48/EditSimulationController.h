/*
*
* @author Vitor Moreira <1130564@isep.ipp.pt>
* @author Paulo Andrade <1130313@isep.ipp.pt>
* @author Emanuel Marques <1130553@isep.ipp.pt>
* @author Ivo Joao <1130404@isep.ipp.pt>
* @author Aria Jozi <1130777@isep.ipp.pt>
* @version 1.0
*
*/

#ifndef EDITSIMULATIONCONTROLLER_H
#define	EDITSIMULATIONCONTROLLER_H

#include "Simulation.h"


using namespace std;

class EditSimulationController {
private:

    System * sistema;
    Simulation * ss;

public:
    
    EditSimulationController();
    EditSimulationController(System * sistema);
    ~EditSimulationController();
    void setName(string name);
    void setStart(double st);
    void setEnd(double st);
    void setStep(double st);
    void resetVelocitysPositions();
    Simulation* getSimulationByName(string name) const ;
    void setSimulation(Simulation* s);
    
};

EditSimulationController::EditSimulationController() {
        
}

EditSimulationController::EditSimulationController(System * sistema){
    this->sistema = sistema;
}

EditSimulationController::~EditSimulationController() {

}

void EditSimulationController::setSimulation(Simulation* s)
{
    this->ss=s;
}
void EditSimulationController::resetVelocitysPositions()
{
    vector<vector<vetor>> matrix;
    //reset a matrix das velocidades e posicoes
	if (this->sistema->getDatabaseConnection()){
		this->sistema->getDatabase()->deleteSimulationVelPos(this->ss);
	}
    this->ss->setVelocitysPositions(matrix);

    
}
void EditSimulationController::setName(string name){

	if (this->sistema->getDatabaseConnection()){
		this->sistema->getDatabase()->editSimulation(this->ss, name, this->ss->getStart_time(), this->ss->getEnd_time(), this->ss->getTime_step());
	}
    this->ss->setName(name);
}
void EditSimulationController::setStart(double st)
{
	if (this->sistema->getDatabaseConnection()){
		this->sistema->getDatabase()->editSimulation(this->ss, this->ss->getName(), st, this->ss->getEnd_time(), this->ss->getTime_step());
	}
    this->ss->setStart_time(st);
}
void EditSimulationController::setEnd(double st)
{
	if (this->sistema->getDatabaseConnection()){
		this->sistema->getDatabase()->editSimulation(this->ss, this->ss->getName(), this->ss->getStart_time(), st, this->ss->getTime_step());
	}
    this->ss->setEnd_time(st);
}
void EditSimulationController::setStep(double st)
{
	if (this->sistema->getDatabaseConnection()){
		this->sistema->getDatabase()->editSimulation(this->ss, this->ss->getName(), this->ss->getStart_time(), this->ss->getEnd_time(), st);
	}
    this->ss->setTime_step(st);
}


#endif	/* EDITSIMULATIONCONTROLLER_H */

