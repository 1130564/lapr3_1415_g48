/*
*
* @author Vitor Moreira <1130564@isep.ipp.pt>
* @author Paulo Andrade <1130313@isep.ipp.pt>
* @author Emanuel Marques <1130553@isep.ipp.pt>
* @author Ivo Joao <1130404@isep.ipp.pt>
* @author Aria Jozi <1130777@isep.ipp.pt>
* @version 1.0
*
*/

#ifndef CSVWRITER_H
#define	CSVWRITER_H

#include <cstdlib>
#include <iostream>
#include <sstream>
#include <vector>
#include <string>
#include <fstream>

using namespace std;

#include "ExportInterface.h"
#include "Simulation.h"

class CsvWriter : public ExportInterface {
private:

public:
    CsvWriter();
    ~CsvWriter();

    void writeVelPosMatrix(const string file, Simulation * simulation);
};

CsvWriter::CsvWriter() : ExportInterface() {

}

CsvWriter::~CsvWriter() {

}

void CsvWriter::writeVelPosMatrix(const string file, Simulation * simulation) {

    ofstream outfile;
    outfile.open(file+".csv");
    if (outfile.is_open()) {

        vector<vector<vetor> > velPos = simulation->getVelocitysPositions();

        list<Body *> objects = simulation->getBodies_subset();

        // escrever nomes dos planetas
        for (list<Body *>::iterator itr = objects.begin(); itr != objects.end(); ++itr) {

            outfile << (*(*itr)).getName();
            outfile << ";";
        }

        outfile << endl;

        // escrever velocidades e posiçoes a partir da matriz
        for (int i = 0; i < velPos[0].size(); i += 2) {

            for (vector<vector<vetor> >::iterator it = velPos.begin(); it != velPos.end(); ++it) {

                vector<vetor> aux = (*it);
                outfile << "v[" << aux[i].x << "," << aux[i].y << "," << aux[i].z << "];";
            }
            outfile << endl;

            for (vector<vector<vetor> >::iterator it = velPos.begin(); it != velPos.end(); ++it) {

                vector<vetor> aux = (*it);
                outfile << "p[" << aux[i + 1].x << "," << aux[i + 1].y << "," << aux[i + 1].z << "];";
            }
            outfile << endl;

        }

        outfile.close();
    } else {
        cout << "Impossível escrever para esse ficheiro!" << endl;
    }

}

#endif	/* CSVWRITER_H */

