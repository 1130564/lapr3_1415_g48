/*
*
* @author Vitor Moreira <1130564@isep.ipp.pt>
* @author Paulo Andrade <1130313@isep.ipp.pt>
* @author Emanuel Marques <1130553@isep.ipp.pt>
* @author Ivo Joao <1130404@isep.ipp.pt>
* @author Aria Jozi <1130777@isep.ipp.pt>
* @version 1.0
*
*/

#ifndef EXPORTRESULTSTOCSVUI_H
#define	EXPORTRESULTSTOCSVUI_H

#include "ExportResultsToCSVController.h"


using namespace std;

class ExportResultsToCSVUI {
public:
    ExportResultsToCSVUI(System * sistema);
    ~ExportResultsToCSVUI();

    void run();
    bool pedeConfirmacao() const;

private:
    System * sistema;
    ExportResultsToCSVController controller;

};

ExportResultsToCSVUI::ExportResultsToCSVUI(System * sistema) : controller(sistema) {
    this->sistema = sistema;
}

ExportResultsToCSVUI::~ExportResultsToCSVUI() {
}

bool ExportResultsToCSVUI::pedeConfirmacao() const {

    string resposta;
    do {
        cout << "\nSimulation: " << this->controller.getSimulation()->getName() << endl;
        cout << "File name: " << this->controller.getFileName() << endl;
        cout << "\nDo you want to export the results of this simulation to this file? y/n " << endl;
        cin >> resposta;

    } while (resposta != "y" && resposta != "n");

    if (resposta == "y") {
        return true;
    } else {
        return false;
    }

}

void ExportResultsToCSVUI::run() {
    
    list<Simulation*> simulations = this->controller.getSimulationsList();
    vector<Simulation*> temp;
    int i = 1;
    int resposta;
    for(list<Simulation*>::iterator itr = simulations.begin(); itr != simulations.end(); ++itr) {
        temp.push_back(*itr);
        cout << i << ": " << (*itr)->getName() << endl;
    }
    
    do {
        cout << "\nChoose the number of the simulation: " << endl;
        cin >> resposta;

    } while (resposta < 0 && resposta > simulations.size());
    
    this->controller.setSimulation(temp[resposta-1]);
    
    string fileName;
    cout << "\nWrite the name of the file with csv extension: " << endl;
    cin >> fileName;
    this->controller.setFileName(fileName);
    
    if(this->pedeConfirmacao()) {
        this->controller.exportResults();
        cout << "Sucess!" << endl;
		char s = getchar();
    } else {
        cout << "Export of results to CSV canceled!" << endl;
		char s = getchar();
    }

}

#endif	/* EXPORTRESULTSTOCSVUI_H */

