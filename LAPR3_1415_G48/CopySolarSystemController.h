﻿/*
*
* @author Vitor Moreira <1130564@isep.ipp.pt>
* @author Paulo Andrade <1130313@isep.ipp.pt>
* @author Emanuel Marques <1130553@isep.ipp.pt>
* @author Ivo Joao <1130404@isep.ipp.pt>
* @author Aria Jozi <1130777@isep.ipp.pt>
* @version 1.0
*
*/

#ifndef COPYSOLARSYSTEMCONTROLLER_H
#define	COPYSOLARSYSTEMCONTROLLER_H

#include <list>

#include "Body.h"
#include "SolarSystem.h"
#include "System.h"

class CopySolarSystemController {
private:
    
    System * sistema;
    SolarSystem * ss;
    
public:
    
    CopySolarSystemController(System * s);
    CopySolarSystemController();
    ~CopySolarSystemController();
    
    void createNewSolarSystem(SolarSystem* base);
    void mostraNovoSistemaSolar();
    void confirma();

};

CopySolarSystemController::CopySolarSystemController(System * sistema) {
    
    this->sistema = sistema;
}

CopySolarSystemController::CopySolarSystemController() {
	
}

CopySolarSystemController::~CopySolarSystemController() {

}

void CopySolarSystemController::createNewSolarSystem(SolarSystem * base) {
    ss = new SolarSystem(*base);
}
void CopySolarSystemController::mostraNovoSistemaSolar()
{
    cout << * this->ss << endl;
}
void CopySolarSystemController::confirma()
{
    this->sistema->addSolarSystem(this->ss);
}
#endif	/* COPYSOLARSYSTEMCONTROLLER_H */

