/*
 * File:   StarTest.h
 * Author: aria
 *
 * Created on Jan 15, 2015, 11:35:54 PM
 */

#ifndef STARTEST_H
#define	STARTEST_H

#include <cppunit/extensions/HelperMacros.h>

class StarTest : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(StarTest);

    CPPUNIT_TEST(testEscreve);
    CPPUNIT_TEST(testGetID);
    CPPUNIT_TEST(testGetMass);
    CPPUNIT_TEST(testGetName);

    CPPUNIT_TEST_SUITE_END();

public:
    StarTest();
    virtual ~StarTest();
    void setUp();
    void tearDown();

private:
    void testEscreve();
    void testGetID();
    void testGetMass();
    void testGetName();

};

#endif	/* STARTEST_H */

