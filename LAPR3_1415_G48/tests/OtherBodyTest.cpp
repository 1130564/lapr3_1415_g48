/*
 * File:   OtherBodyTest.cpp
 * Author: aria
 *
 * Created on Jan 16, 2015, 4:46:06 PM
 */

#include "OtherBodyTest.h"
#include "OtherBody.h"


CPPUNIT_TEST_SUITE_REGISTRATION(OtherBodyTest);

OtherBodyTest::OtherBodyTest() {
}

OtherBodyTest::~OtherBodyTest() {
}

void OtherBodyTest::setUp() {
}

void OtherBodyTest::tearDown() {
}

void OtherBodyTest::testEscreve() {
    vetor p{1,1,1};
    vetor v{1,1,1};
    OtherBody otherBody("test",1,p,v);
    stringstream valor;
        valor<<"Name: "<<otherBody.getName() << endl;
        valor<<"Name: "<<Body. << endl;
    valor << "Mass: " << Body.getMass() << endl;
    valor << "Position x: " << Body.getInitialPosition().x << endl;
    valor << "Position y: " << Body.getInitialPosition().y << endl;
    valor << "Position z: " << Body.getInitialPosition().z << endl;
    valor << "Velocity x: " << Body.getInitialVelocity().x << endl;
    valor << "Velocity y: " << Body.getInitialVelocity().y << endl;
    valor << "Velocity z: " << Body.getInitialVelocity().z << endl;
    stringstream saida;
    otherBody.escreve(saida);
   
        CPPUNIT_ASSERT(saida.str()== valor.str());
    
        
}

