/*
 * File:   PlanetTest.cpp
 * Author: aria
 *
 * Created on Jan 16, 2015, 5:52:23 PM
 */

#include "PlanetTest.h"
#include "Planet.h"


CPPUNIT_TEST_SUITE_REGISTRATION(PlanetTest);

PlanetTest::PlanetTest() {
}

PlanetTest::~PlanetTest() {
}

void PlanetTest::setUp() {
}

void PlanetTest::tearDown() {
}

void PlanetTest::testEscreve() {
    vetor p{1,1,1};
    vetor v{2,2,2};
    Planet planet("test",2,p,v,1,2,1,2,2,1,2,1,2,1,2,2);
    stringstream valor;
    valor<<"Planet"<< endl;
    valor << "\nName: " << Body.getName()<< endl;
    valor << "Mass: " << Body.getMass() << endl;
    valor << "Position x: " << Body.getInitialPosition().x << endl;
    valor << "Position y: " << Body.getInitialPosition().y << endl;
    valor << "Position z: " << Body.getInitialPosition().z << endl;
    valor << "Velocity x: " << Body.getInitialVelocity().x << endl;
    valor << "Velocity y: " << Body.getInitialVelocity().y << endl;
    valor << "Velocity z: " << Body.getInitialVelocity().z << endl;
    valor<<"Period of Revolution: "<<NaturalSatellite.getPeriodOfRevolution() << endl;
    valor<<"Orbital speed: "<<NaturalSatellite.getOrbitalSpeed() << endl;
    valor<<"Inclination of axis to orbi: "<<NaturalSatellite.getInclOfAxisToOrbit() << endl;
    valor<<"Equatorial diameter: "<<NaturalSatellite.getEquaDiameter() << endl;
    valor<<"Density: "<<NaturalSatellite.getDensity()<< endl;
    valor<<"Escape Velocity: "<<NaturalSatellite.getEscapeVelocity()<< endl;
    valor<<"Semimajor axis: "<<NaturalSatellite.getSemimajorAxis()<< endl;
    valor<<"Orbit eccentricity: "<<NaturalSatellite.getOrbitEccentricity()<< endl;
    valor<<"Orbit inclination: "<<NaturalSatellite.getOrbitInclination()<< endl;
    valor<<"Perihelion: "<<NaturalSatellite.getPerihelion()<< endl;
    valor<<"Aphelion: "<<NaturalSatellite.getAphelion()<< endl;
    stringstream saida;
    planet.escreve(saida); 
    
    
        CPPUNIT_ASSERT(saida.str()== valor.str());
    
   
    
        
    
}

