/*
 * File:   moonTest.cpp
 * Author: aria
 *
 * Created on Jan 15, 2015, 10:59:34 PM
 */

#include "moonTest.h"

#include "Moon.h"


CPPUNIT_TEST_SUITE_REGISTRATION(moonTest);

moonTest::moonTest() {
}

moonTest::~moonTest() {
}

void moonTest::setUp() {
}

void moonTest::tearDown() {
}

void moonTest::testEscreve() {
    
    Moon moon;
    vetor p{1,1,1};
    vetor v{2,2,2};
    Moon moon("name test", 2,p,v,2,1,2,1,2,2,1,2,2,2,2);
    stringstream valor;
    valor<<"Moon"<< endl;
    valor<<"Period of Revolution: "<<NaturalSatellite.getPeriodOfRevolution() << endl;
    valor<<"Orbital speed: "<<NaturalSatellite.getOrbitalSpeed() << endl;
    valor<<"Inclination of axis to orbi: "<<NaturalSatellite.getInclOfAxisToOrbit() << endl;
    valor<<"Equatorial diameter: "<<NaturalSatellite.getEquaDiameter() << endl;
    valor<<"Density: "<<NaturalSatellite.getDensity()<< endl;
    valor<<"Escape Velocity: "<<NaturalSatellite.getEscapeVelocity()<< endl;
    valor<<"Semimajor axis: "<<NaturalSatellite.getSemimajorAxis()<< endl;
    valor<<"Orbit eccentricity: "<<NaturalSatellite.getOrbitEccentricity()<< endl;
    valor<<"Orbit inclination: "<<NaturalSatellite.getOrbitInclination()<< endl;
    valor<<"Perihelion: "<<NaturalSatellite.getPerihelion()<< endl;
    valor<<"Aphelion: "<<NaturalSatellite.getAphelion()<< endl;
    stringstream saida;
    moon.escreve(saida); 
    
    
        CPPUNIT_ASSERT(saida.str()== valor.str());
    
}

