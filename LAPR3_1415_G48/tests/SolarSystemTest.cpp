/*
 * File:   SolarSystemTest.cpp
 * Author: aria
 *
 * Created on Jan 16, 2015, 4:27:36 PM
 */

#include "SolarSystemTest.h"
#include "SolarSystem.h"


CPPUNIT_TEST_SUITE_REGISTRATION(SolarSystemTest);

SolarSystemTest::SolarSystemTest() {
}

SolarSystemTest::~SolarSystemTest() {
}

void SolarSystemTest::setUp() {
}

void SolarSystemTest::tearDown() {
}

void SolarSystemTest::testEscreve() {
  
    SolarSystem solarSystem(12,"test");
    stringstream valor;
        valor<<"Name: "<<solarSystem.getName() << endl;
        
    stringstream saida;
    solarSystem.escreve(saida);
   
        CPPUNIT_ASSERT(saida.str()== valor.str());
    
}

void SolarSystemTest::testGetID() {
    SolarSystem solarSystem;
    int id  = solarSystem.getID();
   
        CPPUNIT_ASSERT(id != NULL);
    
}

void SolarSystemTest::testGetName() {
    SolarSystem solarSystem;
    string res = "teste";
    solarSystem.setName(res);
        CPPUNIT_ASSERT(solarSystem.getName()==res);
    
}

void SolarSystemTest::testGetObjects() {
    SolarSystem solarSystem;
    list<Body*> res= new Body;
    solarSystem.setObjects(res);
    
        CPPUNIT_ASSERT(solarSystem.getObjects()=res);
    
}

void SolarSystemTest::testGetStar() {
    SolarSystem solarSystem;
    Star* res = new Star("name",1);
    solarSystem.setStar(res);
    
        CPPUNIT_ASSERT(solarSystem.getStar()==res);
    
}

