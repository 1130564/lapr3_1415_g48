/*
 * File:   SimulationTest.h
 * Author: aria
 *
 * Created on Jan 16, 2015, 3:54:41 PM
 */

#ifndef SIMULATIONTEST_H
#define	SIMULATIONTEST_H

#include <cppunit/extensions/HelperMacros.h>

class SimulationTest : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(SimulationTest);

    CPPUNIT_TEST(testEscreve);
    CPPUNIT_TEST(testGetBodies_subset);
    CPPUNIT_TEST(testGetEnd_time);
    CPPUNIT_TEST(testGetID);
    CPPUNIT_TEST(testGetName);
    CPPUNIT_TEST(testGetSolarSystem);
    CPPUNIT_TEST(testGetStart_time);
    CPPUNIT_TEST(testGetTime_step);
    CPPUNIT_TEST(testGetVelocitysPositions);

    CPPUNIT_TEST_SUITE_END();

public:
    SimulationTest();
    virtual ~SimulationTest();
    void setUp();
    void tearDown();

private:
    void testEscreve();
    void testGetBodies_subset();
    void testGetEnd_time();
    void testGetID();
    void testGetName();
    void testGetSolarSystem();
    void testGetStart_time();
    void testGetTime_step();
    void testGetVelocitysPositions();

};

#endif	/* SIMULATIONTEST_H */

