/*
 * File:   StarTest.cpp
 * Author: aria
 *
 * Created on Jan 15, 2015, 11:35:54 PM
 */

#include "StarTest.h"
#include "Star.h"


CPPUNIT_TEST_SUITE_REGISTRATION(StarTest);

StarTest::StarTest() {
}

StarTest::~StarTest() {
}

void StarTest::setUp() {
}

void StarTest::tearDown() {
}

void StarTest::testEscreve() {
    
    Star star("name",2);
    stringstream valor;
    valor << "Star" << endl;
    valor<<"Name: "<<star.getName() << endl;
    valor<<"Mass: "<<star.getMass() << endl;
    stringstream saida;
    star.escreve(saida);
    CPPUNIT_ASSERT(saida.str()== valor.str());    
}

void StarTest::testGetID() {
    Star star;
    
    
        CPPUNIT_ASSERT(star.getID()!= NULL);
    }


void StarTest::testGetMass() {
    Star star;
    float res = 2;
    star.setMass(res);
        CPPUNIT_ASSERT(star.getMass()== res);
    
}

void StarTest::testGetName() {
    Star star;
    string res = "test name";
    star.setName(res);
    
        CPPUNIT_ASSERT(star.getName()==res);
    
}

