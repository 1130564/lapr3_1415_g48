/*
 * File:   SimulationTest.cpp
 * Author: aria
 *
 * Created on Jan 16, 2015, 3:54:42 PM
 */

#include "SimulationTest.h"
#include "Simulation.h"


CPPUNIT_TEST_SUITE_REGISTRATION(SimulationTest);

SimulationTest::SimulationTest() {
}

SimulationTest::~SimulationTest() {
}

void SimulationTest::setUp() {
}

void SimulationTest::tearDown() {
}

void SimulationTest::testEscreve() {
   
    Body body=new Body;
    list<Body*> res = {body};
    
    Simulation simulation("name",res,2,2,2);
        stringstream valor;
        valor<<"Name: "<<simulation.getName() << endl;
    
    stringstream saida;
    simulation.escreve(saida);
   
        CPPUNIT_ASSERT(saida.str()== valor.str());
    
}

void SimulationTest::testGetBodies_subset() {
    Simulation simulation;
    Body body=new Body;
    list<Body*> res = {body};
    simulation.setBodies_subset(res);
    
        CPPUNIT_ASSERT(simulation.getBodies_subset()==res);
    
}

void SimulationTest::testGetEnd_time() {
    Simulation simulation;
    double res = 5;
    simulation.setEnd_time(res);
        CPPUNIT_ASSERT(simulation.getEnd_time()==res);
    
}

void SimulationTest::testGetID() {
    Simulation simulation;
    int id=simulation.getID();
        CPPUNIT_ASSERT(id =! NULL);
    
}

void SimulationTest::testGetName() {
    Simulation simulation;
    string res = "test";
    simulation.setName(res);
        CPPUNIT_ASSERT(simulation.getName()==res);
    
}

void SimulationTest::testGetSolarSystem() {
    Simulation simulation;
    SolarSystem* res =new SolarSystem;
    simulation.setSolarSystem(res)
        CPPUNIT_ASSERT(simulation.getSolarSystem()==res);
    
}

void SimulationTest::testGetStart_time() {
    Simulation simulation;
    double res = 5;
   simulation.setStart_time(res);
        CPPUNIT_ASSERT(simulation.getStart_time()==res);
    
}

void SimulationTest::testGetTime_step() {
    Simulation simulation;
    double res = 3;
    simulation.setTime_step(res);
    
        CPPUNIT_ASSERT(simulation.getTime_step()==res);
    
}

void SimulationTest::testGetVelocitysPositions() {
    Simulation simulation;
    vector<vector<vetor> > val ;
    vetor a = {1,2,3};
    vector<vetor> b;
    b.push_back(a);
    val.push_back(b);
    simulation.setVelocitysPositions(val);
   
        CPPUNIT_ASSERT(simulation.getVelocitysPositions()==val);
    
}

