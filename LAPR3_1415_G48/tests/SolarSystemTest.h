/*
 * File:   SolarSystemTest.h
 * Author: aria
 *
 * Created on Jan 16, 2015, 4:27:36 PM
 */

#ifndef SOLARSYSTEMTEST_H
#define	SOLARSYSTEMTEST_H

#include <cppunit/extensions/HelperMacros.h>

class SolarSystemTest : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(SolarSystemTest);

    CPPUNIT_TEST(testEscreve);
    CPPUNIT_TEST(testGetID);
    CPPUNIT_TEST(testGetName);
    CPPUNIT_TEST(testGetObjects);
    CPPUNIT_TEST(testGetStar);

    CPPUNIT_TEST_SUITE_END();

public:
    SolarSystemTest();
    virtual ~SolarSystemTest();
    void setUp();
    void tearDown();

private:
    void testEscreve();
    void testGetID();
    void testGetName();
    void testGetObjects();
    void testGetStar();

};

#endif	/* SOLARSYSTEMTEST_H */

