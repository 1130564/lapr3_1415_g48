/*
*
* @author Vitor Moreira <1130564@isep.ipp.pt>
* @author Paulo Andrade <1130313@isep.ipp.pt>
* @author Emanuel Marques <1130553@isep.ipp.pt>
* @author Ivo Joao <1130404@isep.ipp.pt>
* @author Aria Jozi <1130777@isep.ipp.pt>
* @version 1.0
*
*/

#ifndef EXPORTRESULTSTOCSVCONTROLLER_H
#define	EXPORTRESULTSTOCSVCONTROLLER_H

#include "System.h"


class ExportResultsToCSVController {
public:
    ExportResultsToCSVController(System * sistema);
    ~ExportResultsToCSVController();
    
    list<Simulation*> getSimulationsList() const;
    void setSimulation(Simulation * simulation);
    Simulation* getSimulation() const;
    void exportResults();
    void setFileName(string fileName);
    string getFileName() const;
    
private:
    System * sistema;
    Simulation * simulation;
    string fileName;

};

ExportResultsToCSVController::ExportResultsToCSVController(System * sistema) {
    this->sistema = sistema;
}

ExportResultsToCSVController::~ExportResultsToCSVController() {
}

list<Simulation*> ExportResultsToCSVController::getSimulationsList() const {
    return this->sistema->getSimulations();
}

void ExportResultsToCSVController::setSimulation(Simulation* simulation) {
    this->simulation = simulation;
}

Simulation* ExportResultsToCSVController::getSimulation() const {
    return this->simulation;
}

void ExportResultsToCSVController::exportResults() {
    ExportInterface * exp = new CsvWriter();
    exp->writeVelPosMatrix(this->fileName, this->simulation);
}

void ExportResultsToCSVController::setFileName(string fileName) {
    this->fileName = fileName;
}

string ExportResultsToCSVController::getFileName() const {
    return this->fileName;
}
#endif	/* EXPORTRESULTSTOCSVCONTROLLER_H */

