/*
*
* @author Vitor Moreira <1130564@isep.ipp.pt>
* @author Paulo Andrade <1130313@isep.ipp.pt>
* @author Emanuel Marques <1130553@isep.ipp.pt>
* @author Ivo Joao <1130404@isep.ipp.pt>
* @author Aria Jozi <1130777@isep.ipp.pt>
* @version 1.0
*
*/

#ifndef SIMULATIONUI_H
#define SIMULATIONUI_H

#include "CreateNewSimulationUI.h"
#include "RunSimulationUI.h"
#include "DeleteSimulationUI.h"
#include "CopySimulationUI.h"

class SimulationUI {
public:
    SimulationUI(System * sistema);
    SimulationUI();
    ~SimulationUI();

    int menuSimulation();
    bool caseSimulation(int opc);

    void run();

private:
    System * sistema;
};

SimulationUI::SimulationUI(System * sistema) {
    this->sistema = sistema;
}

SimulationUI::SimulationUI() {

}

SimulationUI::~SimulationUI() {
}

int SimulationUI::menuSimulation() {

    int op;
    cout << "Select an option\n\n"
            "1 - Create new Simulation\n"
            "2 - Create a copy from an existing Simulation\n"
            "3 - Edit a Simulation\n"
            "4 - Delete a Simulation\n"
            "5 - Run a Simulation\n"
            "6 - Exit\n" << endl;
    cin >> op;
    return op;

}

bool SimulationUI::caseSimulation(int opc) {
    switch (opc) {

        case 1:
        {
            CreateNewSimulationUI ui = CreateNewSimulationUI(this->sistema);
            ui.run();
            break;
        }
        case 2:
        {
            CopySimulationUI ui(this->sistema);
            ui.run();
            break;
        }
        case 3:
        {
            EditSimulationUI ui(this->sistema);
            ui.run();
            break;
        }
            //chamar UC para editar uma simula�ao
            break;
        case 4:
        {
            DeleteSimulationUI ui = DeleteSimulationUI(this->sistema);
            ui.run();
            break;
        }
        case 5:
        {
            RunSimulationUI ui = RunSimulationUI(this->sistema);
            ui.run();
            break;
        }
        case 6:
            //Exit case. Doesn't call anything and exits the case.
            break;

        default:
            cout << "Invalid Option!" << endl;
            break;

    }
    return true;

}

void SimulationUI::run() {

    int a = menuSimulation();
    caseSimulation(a);
}

#endif