/* 
 * File:   CopySimulationController.h
 * Author: Paulo
 *
 * Created on 16 de Janeiro de 2015, 18:51
 */

#ifndef COPYSIMULATIONCONTROLLER_H
#define	COPYSIMULATIONCONTROLLER_H

#include "System.h"
#include "EditSimulationUI.h"

class CopySimulationController {
private:

    System * sistema;
    Simulation * s;

public:

    CopySimulationController(System * s);
    ~CopySimulationController();

    void createNewSimulation(Simulation * base);
    void mostraNovaSimulacao();
    void alterarDados();
    void confirma();

};

CopySimulationController::CopySimulationController(System * sistema) {

    this->sistema = sistema;
}

CopySimulationController::~CopySimulationController() {

}

void CopySimulationController::createNewSimulation(Simulation * base) {
	this->s = new Simulation;
	s->setName(base->getName());
	s->setEnd_time(base->getEnd_time());
	s->setStart_time(base->getStart_time());
	s->setTime_step(base->getTime_step());
	vector<vector < vetor>> vazio;
	s->setVelocitysPositions(vazio);
	s->setBodies_subset(base->getBodies_subset());
	if (this->sistema->getDatabaseConnection()){
		s->setSolarSystem(this->sistema->getDatabase()->getSolarSystemBySimulationID(base->getID()));
	}
	else{
		s->setSolarSystem(base->getSolarSystem());
	}
}

void CopySimulationController::mostraNovaSimulacao() {
    cout << *this->s << endl;
}

void CopySimulationController::confirma() {
	if (this->sistema->getDatabaseConnection()){
		this->sistema->getDatabase()->pushSimulation(this->s);
	}
    this->sistema->addSimulation(this->s);
}
void CopySimulationController::alterarDados()
{
    EditSimulationUI ui(this->sistema);
    ui.alterarDados(this->s);
}
#endif	/* COPYSIMULATIONCONTROLLER_H */
