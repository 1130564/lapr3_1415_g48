/*
*
* @author Vitor Moreira <1130564@isep.ipp.pt>
* @author Paulo Andrade <1130313@isep.ipp.pt>
* @author Emanuel Marques <1130553@isep.ipp.pt>
* @author Ivo Joao <1130404@isep.ipp.pt>
* @author Aria Jozi <1130777@isep.ipp.pt>
* @version 1.0
*
*/

#ifndef STAR_H
#define	STAR_H

using namespace std;

class Star {
private:

    // name
    string name;
    // star's mass
    float mass;
	int id;

public:

    Star();
	Star(int id, string name, float mass);
    Star(string name, float mass);
    Star(const Star& star);
    ~Star();
    Star * clone() const;

	int getID() const;
    string getName() const;
    float getMass() const;

    void setName(string name);
    void setMass(float mass);

    void escreve(ostream& ostr) const;

};

Star::Star() {
	this->id = newStar();
    this->name = "not defined";
    this->mass = 0;
}

Star::Star(int id1, string name, float mass) {
	this->id = id1;
	this->name = name;
	this->mass = mass;
}
Star::Star(string name, float mass) {
	this->id = newStar();
    this->name = name;
    this->mass = mass;
}

Star::Star(const Star& star) {
	this->id = newStar();
    this->name = star.name;
    this->mass = star.mass;
}

Star::~Star() {

}

Star * Star::clone() const {
    return new Star(*this);
}

void Star::setMass(float mass) {
    this->mass = mass;
}

float Star::getMass() const {
    return mass;
}

void Star::setName(string name) {
    this->name = name;
}

string Star::getName() const {
    return name;
}
int Star::getID() const{
	return this->id;
}

void Star::escreve(ostream& ostr) const {
    ostr << "Star" << endl;
    ostr << "Name: " << this->name << endl;
    ostr << "Mass: " << this->mass << endl;
}

ostream& operator<<(ostream& ostr, const Star &b) {
    b.escreve(ostr);
    return ostr;
}

#endif	/* STAR_H */

