﻿/*
*
* @author Vitor Moreira <1130564@isep.ipp.pt>
* @author Paulo Andrade <1130313@isep.ipp.pt>
* @author Emanuel Marques <1130553@isep.ipp.pt>
* @author Ivo Joao <1130404@isep.ipp.pt>
* @author Aria Jozi <1130777@isep.ipp.pt>
* @version 1.0
*
*/

#ifndef SOLARSYSTEMUI_H
#define SOLARSYSTEMUI_H

#include "CreateNewSolarSystemUI.h"
#include "EditSolarSystemUI.h"
#include "CopySolarSystemUI.h"
#include "OpenSolarSystemUI.h"

using namespace std;

class SolarSystemUI {
public:
    SolarSystemUI(System * sistema);
    ~SolarSystemUI();

    void run();

    int menuSolar();
    bool caseSolar(int opc);


private:
    System * sys;
};

SolarSystemUI::SolarSystemUI(System * sistema) {
    this->sys = sistema;
}

SolarSystemUI::~SolarSystemUI() {
}

int SolarSystemUI::menuSolar() {

    int op;
    cout << "Select an option\n\n"
            "1 - Open an existing Solar System\n"
            "2 - Create a new Solar System\n"
            "3 - Create a copy from an existing Solar System\n"
            "4 - Edit a Solar System\n"
            "5 - Exit\n" << endl;
    cin >> op;
    return op;

}

bool SolarSystemUI::caseSolar(int opc) {
    switch (opc) {

		case 1:
		{
			OpenSolarSystemUI ui(this->sys);
			ui.run();
			break;
		}
        case 2:
        {
            CreateNewSolarSystemUI ui = CreateNewSolarSystemUI(this->sys);
            ui.run();
            break;
        }
        case 3:
        {
            CopySolarSystemUI ui = CopySolarSystemUI(this->sys);
            ui.run();
            break;
        }
        case 4:
        {
            EditSolarSystemUI ui = EditSolarSystemUI(this->sys);
            ui.run();
            break;
        }
        case 5:
            //Exit case. Doesn't call anything and exits the case.
            break;

        default:
            cout << "Invalid Option!" << endl;
            break;

    }
    return true;

}

void SolarSystemUI::run() {
    int a = menuSolar();
    caseSolar(a);

}

#endif