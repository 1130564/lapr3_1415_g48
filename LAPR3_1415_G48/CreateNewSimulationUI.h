/*
 *
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Ivo Joao <1130404@isep.ipp.pt>
 * @author Aria Jozi <1130777@isep.ipp.pt>
 * @version 1.0
 *
 */

#ifndef CREATENEWSIMULATIONUI_H
#define CREATENEWSIMULATIONUI_H

#include "CreateNewSimulationController.h"
#include "Planet.h"
#include "System.h"
#include "CsvWriter.h"
#include "HTMLWriter.h"

class CreateNewSimulationUI {
public:
    CreateNewSimulationUI(System * sistema);
    ~CreateNewSimulationUI();

    bool pedeConfirmacao() const;
    void run();

private:
    System * sistema;
    CreateNewSimulationController controller;

};

CreateNewSimulationUI::CreateNewSimulationUI(System * sistema) : controller(sistema) {
    this->sistema = sistema;
}

CreateNewSimulationUI::~CreateNewSimulationUI() {
}

bool CreateNewSimulationUI::pedeConfirmacao() const {

    string resposta;
    do {
        cout << "\nSimulation's name: " << this->controller.getName() << endl;
        cout << "Simulation's start time: " << this->controller.getStart() << endl;
        cout << "Simulation's end time: " << this->controller.getEnd() << endl;
        cout << "Simulation's time step: " << this->controller.getStep() << endl;
        cout << "Simulation's solar system: " << this->controller.getSolarSystem()->getStar()->getName() << endl;
        cout << "Simulation's bodies: " << endl;
        list<Body*> lista = controller.getBodies();
        for(list<Body*>::iterator itr = lista.begin(); itr != lista.end(); ++itr) {
            cout << (*itr)->getName() << endl;
        }

        cout << "\nDo you confirm the creation of the simulation? y/n" << endl;
        cin >> resposta;

    } while (resposta != "y" && resposta != "n");

    if (resposta == "y") {
        return true;
    } else {
        return false;
    }

}

void CreateNewSimulationUI::run() {

    Simulation * simulation = controller.createSimulation();
    controller.setSimulation(simulation);

    cout << "\nInsert the simulation's name:" << endl;
    string name;
    cin >> name;
    controller.setName(name);

    cout << "\nInsert the simulation's start time:" << endl;
    double start_time;
    cin >> start_time;
    controller.setStart(start_time);

    cout << "\nInsert the simulation's end time:" << endl;
    double end_time;
    cin >> end_time;
    controller.setEnd(end_time);

    cout << "\nInsert the simulation's time step:" << endl;
    double time_step;
    cin >> time_step;
    controller.setStep(time_step);

    cout << "\nSelect the simulation's solar system:" << endl;
    vector<SolarSystem * > sistemassolares;
    int i = 1;
    int resposta = 0;

    list<SolarSystem *> listaSS = this->controller.getSolarSystems();
    for (list<SolarSystem * >::iterator itr = listaSS.begin(); itr != listaSS.end(); ++itr) {
        cout << i << ": " << (*itr)->getName() << endl;
        i++;
        sistemassolares.push_back(*itr);
    }

    cout << "\nChoose the number of the Solar System." << endl;
    cin >> resposta;
    if (resposta != 0 && resposta <= this->sistema->getSolarSystems().size()) {
        controller.setSolarSystem(sistemassolares[resposta - 1]);
    }

    cout << "\nChoose the bodies you wish to be on the simulation:" << endl;
    list<Body*> listaB = controller.getSolarSystemBodies();
    vector<Body*> ssBodies;
    list<Body*> bodies_subset;
    i = 1;
    for (list<Body*>::iterator itr = listaB.begin(); itr != listaB.end(); ++itr) {
        cout << i << ": " << (*itr)->getName() << endl;
        i++;
        ssBodies.push_back(*itr);
    }
    do {
        cout << "\nChoose one or enter 0 do stop: " << endl;
        cin >> resposta;
        if (resposta != 0 && resposta <= ssBodies.size()) {
            bodies_subset.push_back(ssBodies[resposta - 1]);
        } else if (resposta > ssBodies.size()) {
            cout << "\nInvalid option!" << endl;
        }
    } while (resposta != 0);

    controller.setbodies(bodies_subset);

    if (this->pedeConfirmacao()) {
        controller.addSimulation();
		controller.pushToDB();
        cout << "\nSimulation added with sucess!" << endl;
		cout << "Press any key to continue..." << endl;
		char s;
		cin >> s;
    } else {
        cout << "\nCreation of simulation canceled!" << endl;
		cout << "Press any key to continue..." << endl;
		char s;
		cin >> s;
    }

}



#endif
