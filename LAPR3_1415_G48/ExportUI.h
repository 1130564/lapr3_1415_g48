/*
*
* @author Vitor Moreira <1130564@isep.ipp.pt>
* @author Paulo Andrade <1130313@isep.ipp.pt>
* @author Emanuel Marques <1130553@isep.ipp.pt>
* @author Ivo Joao <1130404@isep.ipp.pt>
* @author Aria Jozi <1130777@isep.ipp.pt>
* @version 1.0
*
*/

#ifndef EXPORTUI_H
#define	EXPORTUI_H

#include "ExportResultsToCSVUI.h"
#include "ExportResultsToHTMLUI.h"


class ExportUI {
public:
    ExportUI(System * sistema);
    ExportUI();
    ~ExportUI();

    int menuExport();
    bool caseExport(int opc);

    void run();

private:
    System * sistema;
};

ExportUI::ExportUI(System * sistema) {
    this->sistema = sistema;
}

ExportUI::ExportUI() {

}

ExportUI::~ExportUI() {
}

int ExportUI::menuExport() {

    int op;
    cout << "Select an option\n\n"
            "1 - Export results to HTML\n"
            "2 - Export results to CSV\n"
            "6 - Exit\n" << endl;
    cin >> op;
    return op;

}

bool ExportUI::caseExport(int opc) {
    switch (opc) {

        case 1:
        {
            ExportResultsToHTMLUI ui(sistema);
            ui.run();
            break;
        }
        case 2:
        {
            ExportResultsToCSVUI ui(sistema);
            ui.run();
            break;
        }
        default:
            cout << "Invalid Option!" << endl;
            break;

    }
    return true;

}

void ExportUI::run() {

    int a = menuExport();
    caseExport(a);
}

#endif	/* EXPORTUI_H */

