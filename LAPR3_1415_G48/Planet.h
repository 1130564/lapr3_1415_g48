/*
*
* @author Vitor Moreira <1130564@isep.ipp.pt>
* @author Paulo Andrade <1130313@isep.ipp.pt>
* @author Emanuel Marques <1130553@isep.ipp.pt>
* @author Ivo Joao <1130404@isep.ipp.pt>
* @author Aria Jozi <1130777@isep.ipp.pt>
* @version 1.0
*
*/

#ifndef PLANET_H
#define	PLANET_H

#include "NaturalSatellite.h"
#include "Moon.h"
#include "OtherBody.h"


using namespace std;

class Planet : public NaturalSatellite {
private:

    // the moons orbiting this planet
    list<Moon*> moons;
    // the otherBodys orbiting this planet
    list<OtherBody*> otherBodys;

public:

    Planet();
    Planet(string name, double mass, vetor initialVelocity, vetor initialPosition, double period_of_revolution, double orbital_speed, double incl_of_axis_to_orbit, double equa_diameter, double density, double escape_velocity, double semimajor_axis, double orbit_eccentricity, double orbit_inclination, double perihelion, double aphelion, double meanAnomaly, double longitudeOfPerihelion, double longitudeOfAscendingNode);
	Planet(int id, string name, double mass,
		vetor initialVelocity, vetor initialPosition, double period_of_revolution,
		double orbital_speed, double incl_of_axis_to_orbit, double equa_diameter,
		double density, double escape_velocity, double semimajor_axis, 
		double orbit_eccentricity, double orbit_inclination, double perihelion,
		double aphelion, double meanAnomaly, double longitudeOfPerihelion, double longitudeOfAscendingNode);
    Planet(const Planet& toCopy);
    
    ~Planet();
    Planet * clone() const;

    void escreve(ostream& ostr) const;
    bool addMoons(Moon * m);
    bool addOtherBody(OtherBody * ob);
};

Planet::Planet() : NaturalSatellite(), moons(), otherBodys() {

}

Planet::Planet(string name, double mass, vetor initialVelocity, vetor initialPosition, double period_of_revolution, double orbital_speed, double incl_of_axis_to_orbit, double equa_diameter, double density, double escape_velocity, double semimajor_axis, double orbit_eccentricity, double orbit_inclination, double perihelion, double aphelion, double meanAnomaly, double longitudeOfPerihelion, double longitudeOfAscendingNode)
: NaturalSatellite(name, mass, initialVelocity, initialPosition, period_of_revolution, orbital_speed, incl_of_axis_to_orbit, equa_diameter, density, escape_velocity, semimajor_axis, orbit_eccentricity, orbit_inclination, perihelion, aphelion, meanAnomaly, longitudeOfPerihelion, longitudeOfAscendingNode), moons(), otherBodys() {

}

Planet::Planet(int id1, string name, double mass, vetor initialVelocity, vetor initialPosition, double period_of_revolution, double orbital_speed, double incl_of_axis_to_orbit, double equa_diameter, double density, double escape_velocity, double semimajor_axis, double orbit_eccentricity, double orbit_inclination, double perihelion, double aphelion, double meanAnomaly, double longitudeOfPerihelion, double longitudeOfAscendingNode)
	: NaturalSatellite(id1, name, mass, initialVelocity, initialPosition, period_of_revolution, orbital_speed, incl_of_axis_to_orbit, equa_diameter, density, escape_velocity, semimajor_axis, orbit_eccentricity, orbit_inclination, perihelion, aphelion, meanAnomaly, longitudeOfPerihelion, longitudeOfAscendingNode), moons(), otherBodys() {


}


Planet::Planet(const Planet& toCopy) : NaturalSatellite(toCopy), moons(toCopy.moons), otherBodys(toCopy.otherBodys) {

}

Planet::~Planet() {

}

Planet * Planet::clone() const {
    return new Planet(*this);


}

bool Planet::addMoons(Moon * m) {
    this->moons.push_back(m);
    return true;
}

bool Planet::addOtherBody(OtherBody* ob) {
    this->otherBodys.push_back(ob);
    return true;
}

void Planet::escreve(ostream& ostr) const {
    ostr << "Planet" << endl;
    NaturalSatellite::escreve(ostr);

}

#endif /* PLANET_H */