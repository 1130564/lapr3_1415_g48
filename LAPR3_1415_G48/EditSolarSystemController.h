/*
*
* @author Vitor Moreira <1130564@isep.ipp.pt>
* @author Paulo Andrade <1130313@isep.ipp.pt>
* @author Emanuel Marques <1130553@isep.ipp.pt>
* @author Ivo Joao <1130404@isep.ipp.pt>
* @author Aria Jozi <1130777@isep.ipp.pt>
* @version 1.0
*
*/

#ifndef EDITSOLARSYSTEMCONTROLLER_H
#define	EDITSOLARSYSTEMCONTROLLER_H

#include "SolarSystem.h"
#include "System.h"

class EditSolarSystemController {
private:

    System * sistema;
    SolarSystem * ss;

public:
    
    EditSolarSystemController();
    EditSolarSystemController(System * sistema);
    ~EditSolarSystemController();

	void setSolarSystem(SolarSystem* s);
    void editSolarSystem(SolarSystem * base);
    void changeStarName(string name);
    void changeStarMass(float mass);
    void changeSolarSystemName(string name);
	void addSolarSystem();
    void addBody(Body * b);
	void setObjects(list<Body*> objects);
	void pushToDB();
	SolarSystem* getSolarSystem();
    list < Body * > getBodys();
};

EditSolarSystemController::EditSolarSystemController() {
        
}

EditSolarSystemController::EditSolarSystemController(System * sistema):ss() {
   
    this->sistema = sistema;
}

EditSolarSystemController::~EditSolarSystemController() {

}

void EditSolarSystemController::editSolarSystem( SolarSystem* base) {
	this->ss = base;
}

void EditSolarSystemController::changeStarName(string name) {
	if (this->sistema->getDatabaseConnection()) {
		this->sistema->getDatabase()->editStar(this->ss, name, this->ss->getStar()->getMass());
	}
   this->ss->getStar()->setName(name);
}

void EditSolarSystemController::changeStarMass(float mass) {
	if (this->sistema->getDatabaseConnection()) {
		this->sistema->getDatabase()->editStar(this->ss, this->ss->getStar()->getName(), mass);
	}
    this->ss->getStar()->setMass(mass);

}

void EditSolarSystemController::changeSolarSystemName(string name) {
    this->ss->setName(name);
	if (this->sistema->getDatabaseConnection()) {
		this->sistema->getDatabase()->editSolarSystem(this->ss, name);
	}

}

list < Body * > EditSolarSystemController::getBodys() {
   return this->ss->getObjects();
}

void EditSolarSystemController::addBody(Body * b) {
    this->ss->addObject(b);
}
SolarSystem* EditSolarSystemController::getSolarSystem(){
	return this->ss;
}

void EditSolarSystemController::setSolarSystem(SolarSystem* s){
	this->ss = s;
}
void EditSolarSystemController::addSolarSystem()
{
	this->sistema->addSolarSystem(this->ss);
}

void EditSolarSystemController::setObjects(list<Body*> objects){
	if (this->sistema->getDatabaseConnection()) {
		this->sistema->getDatabase()->deleteObjectsOfSolarSystem(this->ss);
	}
	this->ss->setObjects(objects);
}

void EditSolarSystemController::pushToDB(){
	this->sistema->getDatabase()->pushSolarSystem(this->ss);
	this->sistema->getDatabase()->pushStar(this->ss->getStar(), this->ss->getID());

}


#endif	/* EDITSOLARSYSTEMCONTROLLER_H */

